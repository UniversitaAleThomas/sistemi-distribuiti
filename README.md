# Zombie OutBreak#

### Abstract ###

Progettazione e simulazione della diffusione dell'epidemia Zombie: un soluzione che combina JADE Framework con TuCSoN distributed tuple centres .
Verranno simulate diverse modalità di diffusione di un virus altamente infettivo,  valutando le misure di contenimento utilizzando un modello ad agenti.

### Requisiti ###

Realizzare una simulazione che mostri la diffusione del virus z. La simulazione deve essere temporizzata, distribuita e deve prevedere un'interfaccia grafica di controllo per la gestione delle configurazioni iniziali e per l'analisi dello stato della simulazione. 

Punto cruciale della simulazione saranno le azioni intraprese dal governo centrale per contrastare l'epidemia e l'analisi della sua diffusione tra le diverse città.

### Contacts ###

* Alessandro Neri (alessandro.neri12@studio.unibo.it)
* Thomas Trapanese (thomas.trapanese@studio.unibo.it)