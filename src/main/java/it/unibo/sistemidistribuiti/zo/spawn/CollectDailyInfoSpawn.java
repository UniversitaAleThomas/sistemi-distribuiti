package it.unibo.sistemidistribuiti.zo.spawn;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractSpawnActivity;
import alice.tucson.api.exceptions.TucsonInvalidLogicTupleException;
import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.dao.DailyInfoDAO;
import it.unibo.sistemidistribuiti.zo.event.EventDispatcher;
import it.unibo.sistemidistribuiti.zo.event.dayended.DayEndedEvent;

public class CollectDailyInfoSpawn extends AbstractSpawnActivity {

	private static final long serialVersionUID = -754270056804180286L;
	private static Logger log = LogManager.getLogger();
	
	@Override
	public void doActivity() {
		
		log.info("[CollectDailyInfoSpawn] started");
		
		try {
			
			List<CityDAO> citiesInfo = new ArrayList<CityDAO>();
			
			// lettura del giorno
			LogicTuple eod = rd(LogicTuple.parse("eod(X)"));
			int day = eod.getArg(0).intValue();
			
			// lettura delle info città
			List<LogicTuple> res = rdAll(LogicTuple.parse(CityDAO.TEMPLATE));
			for (LogicTuple tuple : res) {				
				citiesInfo.add(new CityDAO(tuple));
			}
			
			// lettura del numero di persone in viaggio
			// TODO: si blocca qua
			LogicTuple tt = rd(LogicTuple.parse("travellers(X)"));			
			int travelers = tt.getArg(0).intValue();
			
			//lanciamo l'evento day ended
			EventDispatcher.get().fireEvent(new DayEndedEvent(new DailyInfoDAO(day, citiesInfo), travelers));
			
			//AN: salvare questo dato per avere una storia dei giorni passati
//			String outTemplate = "ci(name(N),population(P),infected(I))";
//			out(LogicTuple.parse("dailyInfo(day(" + room + "),tuple(" + res.toString() + "))"));
			
		} catch (InvalidLogicTupleException | TucsonInvalidLogicTupleException e) {
			e.printStackTrace();
		}
		
	}

}
