package it.unibo.sistemidistribuiti.zo.spawn;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractSpawnActivity;
import alice.tucson.api.exceptions.TucsonInvalidLogicTupleException;
import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.dao.ConfigDAO;

public class RiskResponseSpawn extends AbstractSpawnActivity {

	private static final long serialVersionUID = -754270056804180286L;

	private static Logger log = LogManager.getLogger();
	private ConfigDAO config;

	@Override
	public void doActivity() {

		log.info("[RiskResponseSpawn] started");
		
		try {

			LogicTuple tupleConf = LogicTuple.parse(ConfigDAO.TEMPLATE);
			LogicTuple rdConf = rd(tupleConf);
			config = new ConfigDAO(rdConf);

			LogicTuple tuple = LogicTuple.parse(CityDAO.TEMPLATE);
			List<LogicTuple> rdAllCities = rdAll(tuple);

			for (LogicTuple logicTuple : rdAllCities) {
				List<String> actions = checkAction(new CityDAO(logicTuple));
				for (String tupla : actions) {

					if (tupla != null) {
						out(LogicTuple.parse(tupla));
					}
				}
			}

		} catch (InvalidLogicTupleException | TucsonInvalidLogicTupleException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Valutazione della risposta
	 */
	private List<String> checkAction(CityDAO ci) {
		List<String> ret = new ArrayList<>();
		double pc = ci.getInfectedPercentage();
		if (ci.isNuclearized()) {
			return ret;
		}
		if (pc >= config.getAtomicPercentage()) {
			ret.add("atomic_bomb(" + ci.getId() + ")");
			return ret;
		}
		if (pc >= config.getQuarantinePercentage() && !ci.isQuarantine()) {
			ret.add("quarantine(" + ci.getId() + ")");
		} else if (pc < config.getQuarantinePercentage() && ci.isQuarantine()) {
			ret.add("stop_quarantine(" + ci.getId() + ")");
		}
		if (pc >= config.getTaskForcePercentage() && !ci.containsTaskForce()) {
			ret.add("task_force(" + ci.getId() + ")");
		} else if (pc == 0 && ci.containsTaskForce()) {
			ret.add("stop_task_force(" + ci.getId() + ")");
		}
		return ret;
	}

}
