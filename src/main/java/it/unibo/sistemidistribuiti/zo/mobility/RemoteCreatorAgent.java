package it.unibo.sistemidistribuiti.zo.mobility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.sistemidistribuiti.zo.agent.BaseAgent;
import it.unibo.sistemidistribuiti.zo.utils.AgentConstants;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.ContainerID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.domain.JADEAgentManagement.CreateAgent;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.lang.acl.ACLMessage;

/**
 * Secondo me questa classe non ci serve
 * Loro la usavano per creare agenti in remoto
 * @author Ale
 */
public class RemoteCreatorAgent extends BaseAgent implements IRemoteCreatorAgent{

	private static Logger log = LogManager.getLogger();
	
	private static final long serialVersionUID = -5482480057199996117L;

	/*
	 * Agente che crea un agente su un container remoto, le specifiche
	 * dell'agente e del container sono passate come argomenti
	 */

	@Override
	protected void setup() {
		super.setup();
		
		registerDFService(AgentConstants.TYPE_REMOTE_CREATOR);
		/* registriamo l'agente come implementor della seguente interfaccia */
		registerO2AInterface(IRemoteCreatorAgent.class, this);
		
	}

	public void createAgent(String remoteAgentName, String remoteAgentClass, ContainerID destination, Object[] objtab) {
		this.addBehaviour(new RemoteCreatorBehaviour(remoteAgentName, remoteAgentClass, destination, objtab));
	}

	private class RemoteCreatorBehaviour extends OneShotBehaviour {

		private static final long serialVersionUID = -7445032243370275236L;
		
		private String remoteAgentName;
		private String remoteAgentClass;
		private ContainerID destination;
		private Object[] objtab;

		public RemoteCreatorBehaviour(String ran, String rac, ContainerID dest, Object[] ob) {
			remoteAgentName = ran;
			remoteAgentClass = rac;
			destination = dest;
			objtab = ob;
		}

		@Override
		public void action() {

			if(destination == null){
				log.error("Impossibile istanziare l'agente remoto: destination è null");
				return;
			}
			
			try {
				
				// Invia all'AMS una richiesta di creazione di agente su container remoto
				
				getContentManager().registerLanguage(new SLCodec(0));
				getContentManager().registerOntology(JADEManagementOntology.getInstance());

				CreateAgent ca = new CreateAgent();
				ca.setAgentName(remoteAgentName);
				ca.setClassName(remoteAgentClass);
				ca.setContainer(destination);
				for (Object o : objtab) {
					ca.addArguments(o);
				}

				// create and send the message to the ams
				ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
				msg.setOntology(JADEManagementOntology.NAME);
				msg.setLanguage(jade.domain.FIPANames.ContentLanguage.FIPA_SL0);
				msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);

				getContentManager().fillContent(msg, new Action(getAMS(), ca));
				msg.addReceiver(getAMS());
				send(msg);
				
			} catch (CodecException | OntologyException e) {
				e.printStackTrace();
			}

		}

	}
}
