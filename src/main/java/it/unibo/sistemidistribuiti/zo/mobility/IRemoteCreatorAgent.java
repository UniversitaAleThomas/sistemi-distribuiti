package it.unibo.sistemidistribuiti.zo.mobility;

import jade.core.ContainerID;

public interface IRemoteCreatorAgent{

	public void createAgent(String remoteAgentName, String remoteAgentClass, ContainerID destination, Object[] objtab);
}
