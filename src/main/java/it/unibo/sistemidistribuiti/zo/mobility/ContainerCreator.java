package it.unibo.sistemidistribuiti.zo.mobility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.sistemidistribuiti.zo.config.Settings;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.util.ExtendedProperties;
import jade.util.leap.Properties;

public class ContainerCreator {

	private static Logger log = LogManager.getLogger();
	
	public static void main(String[] args) {

		ServerSocket welcomeSocket = null;
		
		try {
			welcomeSocket = new ServerSocket(Settings.CONTAINER_CREATOR_PORT);
			
			String currentHostname = InetAddress.getLocalHost().getHostAddress();
			
			log.info("[" + currentHostname+"] Listening for incoming container request on port " + Settings.CONTAINER_CREATOR_PORT);
			
			while (true) {
				
				Socket connectionSocket = welcomeSocket.accept();
				BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
				
				String request = inFromClient.readLine();
				String[] cmd = request.split("//");
				String mainContainerHost = connectionSocket.getInetAddress().getHostAddress();
				String localhost = connectionSocket.getLocalAddress().getHostAddress();
				
				if (cmd[0].equals("create")) {
					String mainContainerPort = cmd[1];
					String nomePaese = cmd[2];

					log.info("Request received from address " + mainContainerHost + ":" + mainContainerPort);
					
					Properties props = new ExtendedProperties();
					props.setProperty(Profile.LOCAL_HOST, localhost); /* è necessario? */
					props.setProperty(Profile.MAIN_PORT, mainContainerPort);
					props.setProperty(Profile.MAIN_HOST, mainContainerHost);
					props.setProperty(Profile.MAIN, "false"); /* non è un main container */
					props.setProperty(Profile.CONTAINER_NAME, nomePaese);
					props.setProperty(Profile.SERVICES, "it.unibo.tucson.jade.service.TucsonService"); /* enable t4j */

					Profile p = new ProfileImpl(props);
					Runtime.instance().createAgentContainer(p);
				}
				connectionSocket.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(welcomeSocket != null){
				try {
					welcomeSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
