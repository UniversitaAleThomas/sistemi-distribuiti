package it.unibo.sistemidistribuiti.zo.mobility;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ContainerRequester {

	private static Logger log = LogManager.getLogger();
	
	public static void requestContainer(String remoteHost, int remotePort, String nomePaese, int jadeMainContainerPort){
		try {
			Socket clientSocket = new Socket(remoteHost, remotePort);
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			outToServer.writeBytes("create//" + jadeMainContainerPort + "//" + nomePaese);
			clientSocket.close();
			
			log.info("Inviata richiesta di creazione sub container a " + remoteHost + ":" + remotePort);
			
		} catch (IOException e) {
			e.printStackTrace();
			log.error("Fallita richiesta di creazione sub container a " + remoteHost + ":" + remotePort + " - " + e.getMessage());
		}
	}

}
