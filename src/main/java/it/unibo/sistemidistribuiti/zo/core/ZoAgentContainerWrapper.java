package it.unibo.sistemidistribuiti.zo.core;

import it.unibo.sistemidistribuiti.zo.config.Settings;
import it.unibo.sistemidistribuiti.zo.mobility.ContainerRequester;
import it.unibo.sistemidistribuiti.zo.mobility.IRemoteCreatorAgent;
import jade.core.ContainerID;
import jade.core.Profile;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class ZoAgentContainerWrapper {

	public static ZoAgentContainer instance(Profile profile) {
		return new ZoAgentContainerLocale(jade.core.Runtime.instance().createAgentContainer(profile));
	}

	public static ZoAgentContainer instance(String host, int port, String nome, AgentController rca) {
		try {
			return new ZoAgentContainerRemote(host, port, nome, rca);
		} catch (StaleProxyException e) {
			return null;
		}
	}

	/* Wrapper per la creazione di agenti locali */
	public static class ZoAgentContainerLocale implements ZoAgentContainer {
		private AgentContainer container;

		public ZoAgentContainerLocale(AgentContainer container) {
			this.container = container;
		}

		public void createAgent(String agentId, String className, ContainerID destination, Object[] params)
				throws StaleProxyException {
			AgentController ag = container.createNewAgent(agentId, className, params);
			ag.start();
		}
	}

	/* Wrapper per la creazione di agenti remoti */
	public static class ZoAgentContainerRemote implements ZoAgentContainer {
		private IRemoteCreatorAgent remoteCreator;

		public ZoAgentContainerRemote(String host, int port, String nome, AgentController rca)
				throws StaleProxyException {
			// richiedo la creazione del container remoto
			ContainerRequester.requestContainer(host, port, nome, Settings.MC_PORT);
			remoteCreator = (IRemoteCreatorAgent) rca.getO2AInterface(IRemoteCreatorAgent.class);
		}

		public void createAgent(String agentId, String className, ContainerID destination, Object[] params)
				throws StaleProxyException {
			remoteCreator.createAgent(agentId, className, destination, params);
		}
	}

}
