package it.unibo.sistemidistribuiti.zo.core;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;

public interface ISimulator {

	public enum SIM_STATE  {
		IDLE, RUNNING, PAUSED, TERMINATED
	}
	
	/**
	 * *
	 * Avvia la simulazione
	 *
	 * @param countriesMap
	 */
	void start(Map<String, List<CityDAO>> countriesMap, List<InetSocketAddress> listaNodi);

	/**
	 * Avvia l'RMA (Jade Remote Agent Manager)
	 */
	void startRMA();

	/**
	 * Avvia il tucson inspector
	 */
	void startTucsonInspector();

	/**
	 * Ferma la simulazione
	 */
	void stop();

	/**
	 * Sospende la simulazione
	 */
	void pause();

	/**
	 * Riprende la simulazione
	 */
	void resume();

	/**
	 * Riprende la simulazione
	 */
	SIM_STATE getState();

}