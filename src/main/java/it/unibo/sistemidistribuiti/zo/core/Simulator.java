package it.unibo.sistemidistribuiti.zo.core;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.sistemidistribuiti.zo.agent.CityLoaderAgent;
import it.unibo.sistemidistribuiti.zo.agent.CityLoaderAgent.ILoader;
import it.unibo.sistemidistribuiti.zo.agent.GovernmentAgent;
import it.unibo.sistemidistribuiti.zo.agent.SettingsAgent;
import it.unibo.sistemidistribuiti.zo.agent.TimerAgent;
import it.unibo.sistemidistribuiti.zo.config.Settings;
import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.event.EventDispatcher;
import it.unibo.sistemidistribuiti.zo.event.simstatechanged.SimStateChangedEvent;
import it.unibo.sistemidistribuiti.zo.mobility.RemoteCreatorAgent;
import it.unibo.sistemidistribuiti.zo.utils.TucsonUtils;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

/**
 *
 * @author thomas
 */
public class Simulator implements ISimulator, ILoader {

	private SIM_STATE state;
	private AgentController ga, rca;
	private AgentContainer mainContainer;

	private static Logger log = LogManager.getLogger();

	private String tname;

	public Simulator() {
		bootstrap();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unibo.sistemidistribuiti.zo.gui.ISimulator#start(java.util.Map)
	 */
	@Override
	public void start(Map<String, List<CityDAO>> countriesMap, List<InetSocketAddress> listaNodi) {

		if (getState() != SIM_STATE.RUNNING && getState() != SIM_STATE.TERMINATED) {
			try {
				AgentController cla = mainContainer.createNewAgent("cityLoaderAgent", CityLoaderAgent.class.getName(), new Object[]{
						tname,
						countriesMap,
						listaNodi,
						rca, 
						this
				});
				cla.start();
			} catch (StaleProxyException e) {
				log.error("Errore durante le creazione degli agenti: " + e.getMessage());
			}
		}
	}

	@Override
	public void onComplete(int numeroCitta) {
		try {
			AgentController ta = mainContainer.createNewAgent("timerAgent", TimerAgent.class.getName(), new Object[] { tname, numeroCitta });
			ta.start();

			// L'agente governo si occuperà di mandare un messaggio di start
			// a tutti i componenti
			ga = mainContainer.createNewAgent("governmentAgent", GovernmentAgent.class.getName(), new Object[] { tname });
			ga.start();
		} catch (StaleProxyException e) {
			log.error(e);
		}
		setState(SIM_STATE.RUNNING);
	}

	/**
	 * Avvia l'RMA
	 */
	@Override
	public void startRMA() {
		try {
			AgentController rma = mainContainer.createNewAgent("rma", jade.tools.rma.rma.class.getName(),
					new Object[0]);
			rma.start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
			log.error("Launching of rma agent failed");
		}
	}

	/**
	 * Avvia il tucson inspector
	 */
	@Override
	public void startTucsonInspector() {
		TucsonUtils.launchInspector();
	}

	/**
	 * Interrompe la simulazione
	 */
	@Override
	public void stop() {
		if (getState() != SIM_STATE.IDLE) {

			try {
				ga.kill();
			} catch (StaleProxyException e) {
				//do nothing
			}

			log.info("Simulazione terminata correttamente");

			setState(SIM_STATE.TERMINATED);
		}
	}

	/**
	 * Funzione di init del simulatore Prepara la jade platform e il tucson
	 * space
	 */
	private void bootstrap() {

		// ---TUCSON NODE SERVICE---
		tname = TucsonUtils.startNS(Settings.TNS_PORT);

		// ---JADE PLATFORM---
		jade.core.Runtime rt = jade.core.Runtime.instance();

		Profile profile = new ProfileImpl(Settings.MC_HOST, Settings.MC_PORT, null);

		// abilita Tucson4Jade :)
		profile.setParameter(Profile.SERVICES, "it.unibo.tucson.jade.service.TucsonService");

		// DF and AMS are included
		mainContainer = rt.createMainContainer(profile);

		try {

			// lancio il settings agent

			AgentController agSetting = mainContainer.createNewAgent("settingsAgent", SettingsAgent.class.getName(),
					new Object[] { tname });
			agSetting.start();

			// lancio il remote creator agent, in grado di istanzare agenti su
			// nodi remoti

			rca = mainContainer.createNewAgent("remoteCreatorAgent", RemoteCreatorAgent.class.getName(),
					new Object[] { tname });
			rca.start();

		} catch (StaleProxyException e) {
			log.error("Errore durante le creazione dell'agente settings");
		}

		setState(SIM_STATE.IDLE);

		log.info("Bootstrap della simulazione effetturato con successo");
	}

	@Override
	public void pause() {
		if (getState() == SIM_STATE.RUNNING) {
			try {

				// 1. Sospensiamo il governo (che si occuperà di mandare il
				// relativo messaggio di pausa)
				ga.suspend();

				// 2. Impostiamo il cambio di stato
				setState(SIM_STATE.PAUSED);

			} catch (StaleProxyException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void resume() {
		if (getState() == SIM_STATE.PAUSED) {
			try {

				// 1. Riattiviamo il governo (che si occuperà di mandare il
				// relativo messaggio di resume)
				ga.activate();

				// 2. Impostiamo il cambio di stato
				setState(SIM_STATE.RUNNING);

			} catch (StaleProxyException e) {
				e.printStackTrace();
			}
		}
	}

	public SIM_STATE getState() {
		return state;
	}

	protected void setState(SIM_STATE state) {
		// log.info("Cambio stato da " + this.state + " -> " + state);
		this.state = state;
		
		EventDispatcher.get().fireEvent(new SimStateChangedEvent(state));
	}
}
