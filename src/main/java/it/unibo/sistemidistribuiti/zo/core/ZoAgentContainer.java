package it.unibo.sistemidistribuiti.zo.core;

import jade.core.ContainerID;
import jade.wrapper.StaleProxyException;

public interface ZoAgentContainer {
	void createAgent(String agentId, String className, ContainerID destination, Object[] params)
			throws StaleProxyException;
}
