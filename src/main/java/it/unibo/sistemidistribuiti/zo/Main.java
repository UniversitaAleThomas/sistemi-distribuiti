package it.unibo.sistemidistribuiti.zo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.sistemidistribuiti.zo.core.ISimulator;
import it.unibo.sistemidistribuiti.zo.core.Simulator;
import it.unibo.sistemidistribuiti.zo.gui.ControlGUI;
import it.unibo.sistemidistribuiti.zo.mobility.ContainerCreator;

public class Main {
	
	private static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		
		if(args.length == 1 && args[0].equalsIgnoreCase("-remote")){
			log.info("Enabled Remote Mode");
			ContainerCreator.main(new String[]{});
		}else {
			ISimulator sim = new Simulator();
			new ControlGUI(sim);
		}
	}

}
