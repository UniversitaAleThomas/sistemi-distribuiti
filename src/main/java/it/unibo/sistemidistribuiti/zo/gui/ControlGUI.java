package it.unibo.sistemidistribuiti.zo.gui;

import javax.swing.JFrame;

import it.unibo.sistemidistribuiti.zo.core.ISimulator;

/*
 * Finestra principale dell'applicazione.
 */
@SuppressWarnings("serial")
public class ControlGUI extends JFrame {

	public ControlGUI(ISimulator sim) {
		super("Zombie OutBreak control center");

		// Create and set up the window.
		JFrame frame = new MainFrame(sim);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Display the window.
		frame.pack();
		frame.setVisible(true);
	
	}
}
