package it.unibo.sistemidistribuiti.zo.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import it.unibo.sistemidistribuiti.zo.config.Settings;
import it.unibo.sistemidistribuiti.zo.config.SwingLogger;
import it.unibo.sistemidistribuiti.zo.core.ISimulator;
import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.dao.ConfigDAO;
import it.unibo.sistemidistribuiti.zo.dao.DailyInfoDAO;
import it.unibo.sistemidistribuiti.zo.event.EventDispatcher;
import it.unibo.sistemidistribuiti.zo.event.configchange.ConfigChangeEvent;
import it.unibo.sistemidistribuiti.zo.event.dayended.DayEndedEvent;
import it.unibo.sistemidistribuiti.zo.event.dayended.DayEndedEventHandler;
import it.unibo.sistemidistribuiti.zo.event.simstatechanged.SimStateChangedEvent;
import it.unibo.sistemidistribuiti.zo.event.simstatechanged.SimStateChangedEventHandler;

public class MainFrame extends javax.swing.JFrame {

	private static final long serialVersionUID = -4814934779531851804L;

	public DefaultListModel<String> countries;
	public DefaultTableModel countriesTable;

	// Contine l'elenco parziale delle città aggiunte per ogni paese
	public List<CityDAO> tmpCities; 
	public Map<CityDAO, CityPanel> cityPanelMap; // contiene l'assoziazione tra la città (CityInfo) e la sua view

	private String tmpCountryName;
	private ISimulator sim;

	// Oggetti della simulazione
	private Map<String, List<CityDAO>> countriesMap; // Contiene l'elenco delle città diviso per paese
	private List<InetSocketAddress> nodi;

	private static Logger log = (Logger) LogManager.getLogger();

	/**
	 * Creates new form NewJFrame
	 */
	public MainFrame(ISimulator sim) {

		setTitle("Zombie Outbreak 1.0");

		// init dei modelli della sim
		
		this.countries = new DefaultListModel<>();
		this.sim = sim;

		tmpCities = new ArrayList<>();
		cityPanelMap = new HashMap<>();
		countriesMap = new HashMap<>();

		nodi = new ArrayList<InetSocketAddress>();
		nodi.add(new InetSocketAddress(Settings.MC_HOST, Settings.MC_PORT));
		
		// init della grafica
		initComponents();

		// registrazione della text-area logger
		log.getParent().addAppender(SwingLogger.createAppender(taLog, "GUILog", null, null, null));

		// impostazione dei valori di default
		computeGlobalStatistics();

		// gestione degli eventi
		handleSimEvents();
	}

	/**
	 * Gestisce gli eventi interni alla simulazione
	 */
	private void handleSimEvents() {
		// Aggancio l'evento di fine giornata per aggiornare la view
		EventDispatcher.get().addEventHandler(new DayEndedEventHandler() {
			@Override
			public void run(DayEndedEvent dee) {

				SwingUtilities.invokeLater(() -> {

					int curhealthy = 0;
					int curinfected = 0;

					DailyInfoDAO di = dee.getDailyInfo();
					int travelers = dee.getTravelers();

					lblDay.setText("" + di.getDay());

					for (CityDAO ci : di.getCitiesInfo()) {
						cityPanelMap.get(ci).updateData(ci);
						curhealthy += ci.getHealthy();
						curinfected += ci.getInfected();
					}

					computeGlobalStatistics(travelers);

					if (curhealthy == 0 && curinfected == 0) {
						sim.stop();
						JOptionPane.showMessageDialog(MainFrame.this,
								"Il genere umano si è estinto dopo " + di.getDay() + " giorni");
					} else if (curinfected == 0) {
						sim.stop();
						JOptionPane.showMessageDialog(MainFrame.this,
								"L'epidemia zombie è stato contenuta dopo " + di.getDay() + " giorni");
					}

				});

			}

		});

		// Aggancio l'evento di cambio stato della simulazione
		EventDispatcher.get().addEventHandler(new SimStateChangedEventHandler() {

			@Override
			public void run(SimStateChangedEvent event) {
				SwingUtilities.invokeLater(() -> {
					updateBtn();
				});
			}
		});
	}

	/**
	 * Funzione per trigger di evento: config changed
	 */
	private void updateConfig() {
		ConfigDAO config = new ConfigDAO(sldTempo.getValue(), sldQuarantine.getValue(), sldTaskForce.getValue(),
				sldBomb.getValue());
		EventDispatcher.get().fireEvent(new ConfigChangeEvent(config));
	}

	/* STATISTICHE */

	private void computeGlobalStatistics() {
		computeGlobalStatistics(0);
	}

	private void computeGlobalStatistics(int travellers) {

		int healthy = 0;
		int infected = 0;

		if (countriesMap != null) {
			for (Entry<String, List<CityDAO>> e : countriesMap.entrySet()) {
				for (CityDAO ci : e.getValue()) {
					healthy += ci.getHealthy();
					infected += ci.getInfected();
				}
			}
		}

		lblTotaleSani.setText("Totale Sani: " + healthy);
		lblPersoneInViaggio.setText("Persone in viaggio: " + travellers);
		lblTotaleInfetti.setText("Totale Infetti: " + infected);
		lblTotPersone.setText("Totale: " + (healthy + infected + travellers));
	}

	/* LISTENERS */

	private void btnPauseActionPerformed(ActionEvent evt) {
		if (sim.getState() == ISimulator.SIM_STATE.PAUSED) {
			sim.resume();
		} else {
			sim.pause();
		}
	}

	private void btnTucsonInspectorActionPerformed(ActionEvent evt) {
		sim.startTucsonInspector();
	}

	private void btnAggiungiNodoActionPerformed(ActionEvent evt) {

		String ip = JOptionPane.showInputDialog(this, "Inserisci l'indirizzo:porta del nodo", "Indirizzo", JOptionPane.INFORMATION_MESSAGE);
		if ("".equals(ip) || ip == null) {
			return;
		}

		try {

			if (InetAddress.getByName(ip).isReachable(3000)) { // 3 secondi di
																// timeout
				nodi.add(new InetSocketAddress(ip, Settings.CONTAINER_CREATOR_PORT));
				((DefaultListModel<String>) lstNodi.getModel()).addElement(ip);
			} else {
				// l'indirizzo non è raggiungibile
				JOptionPane.showMessageDialog(this, "L'indirizzo " + ip + " non risulta raggiungibile");
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}

	}

	private void btnCreaPaeseActionPerformed(ActionEvent evt) {
		if (!btnCreaPaese.isSelected()) {
			// abbiamo cliccato sul tasto fine inserimento
			countriesMap.put(tmpCountryName, tmpCities);
			countriesTable.addRow(new Object[] { tmpCountryName, tmpCities.size() });
			tmpCities = new ArrayList<>();
		} else {
			// chiediamo il nome del paese
			tmpCountryName = JOptionPane.showInputDialog(this, "Inserisci il nome del paese", "Nome paese",
					JOptionPane.INFORMATION_MESSAGE);
			if ("".equals(tmpCountryName) || tmpCountryName == null) {
				JOptionPane.showMessageDialog(this, "Inserire un nome paese valido");
				btnCreaPaese.setSelected(false);
				return;
			}

			// normalizziamo il nome paese
			tmpCountryName = tmpCountryName.toLowerCase(); // minuscolo
			tmpCountryName = tmpCountryName.replaceAll("\\s", ""); //trim di tutti gli spazi (anche interni)
		}

		// toggle sul tasto di creazione paese
		toggleBtnCreaPaese();

		// aggiorniamo le info sulle statistiche
		computeGlobalStatistics();
	}

	/**
	 * Effettua il toggle sul tasto del paese
	 */
	private void toggleBtnCreaPaese() {
		if (!btnCreaPaese.isSelected()) {
			btnCreaPaese.setText("Crea un paese");
		} else {
			btnCreaPaese.setText("Fine");
		}
	}

	private static final int CI_W = 120;
	private static final int CI_H = 140;

	private void pnlMappaMouseClicked(MouseEvent evt) {
		// Sul click aggiungo un item
		if (!btnCreaPaese.isSelected()) {
			return;
		}
		Random rand = new Random();

		SwingUtilities.invokeLater(() -> {
			// Costruisco la nuova città
			CityDAO newCi = new CityDAO(tmpCountryName + "_" + tmpCities.size(), tmpCountryName,
					(rand.nextInt(10000) + 1) * 100, evt.getPoint());
			// e la sua view
			CityPanel newCityPanel = new CityPanel(newCi);

			newCityPanel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent evt) {
					if (btnPaziente0.isSelected()) {
						newCi.setInfected(1);
						btnPaziente0.setSelected(false);
						newCityPanel.updateData(newCi);
					}
				}
			});
			newCityPanel.setBounds((int) evt.getPoint().getX() - CI_W / 2, (int) evt.getPoint().getY() - CI_H / 2, CI_W,
					CI_H);

			// La aggiungo al pannello (Mappa)
			pnlMappa.add(newCityPanel);
			pnlMappa.validate();
			pnlMappa.repaint();

			// Salvo l'assoziazione citta-view
			cityPanelMap.put(newCi, newCityPanel);
			// Salvo la lista parziale delle città
			tmpCities.add(newCi);
			// Ridisegno la mappa
			pnlMappa.revalidate();

			btnPaziente0.setEnabled(true);
			computeGlobalStatistics();
		});

	}

	private void btnAvviaActionPerformed(ActionEvent evt) {

		if (countriesMap.keySet().size() == 0) {
			JOptionPane.showMessageDialog(this, "Inserisci almeno un paese prima di avviare la simulazione");
			return;
		}

		sim.start(countriesMap, nodi);
	}

	private void btnFermaActionPerformed(ActionEvent evt) {
		sim.stop();
	}

	private void btnRMAActionPerformed(ActionEvent evt) {
		sim.startRMA();
	}

	/**
	 * Aggiorna i btn nella gui
	 */
	private void updateBtn() {
		switch (sim.getState()) {
		case RUNNING:
			btnAvvia.setEnabled(false);
			btnPaziente0.setEnabled(false);
			btnCreaPaese.setEnabled(false);
			btnPause.setEnabled(true);
			btnPause.setText("Pausa");
			btnFerma.setEnabled(true);
			break;
		case PAUSED:
			btnAvvia.setEnabled(false);
			btnPaziente0.setEnabled(false);
			btnCreaPaese.setEnabled(false);
			btnPause.setEnabled(true);
			btnPause.setText("Riprendi");
			btnFerma.setEnabled(true);
			break;
		case IDLE:
			btnAvvia.setEnabled(true);
			btnPaziente0.setEnabled(true);
			btnCreaPaese.setEnabled(true);
			btnPause.setEnabled(false);
			btnPause.setText("Pausa");
			btnFerma.setEnabled(false);
			break;
		case TERMINATED:
			btnAvvia.setEnabled(false);
			btnPaziente0.setEnabled(false);
			btnCreaPaese.setEnabled(false);
			btnPause.setEnabled(false);
			btnPause.setText("Pausa");
			btnFerma.setEnabled(false);
			break;
		}
	}

	/**
	 * Istanzia i componenti della GUI
	 */
	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		// init btns
		initButtons();

		// init labels
		initLabels();

		// init sliders
		initSliders();
		
		// init others components
		initOthers();
		
		// init group layout
		initGroupsLayout();

		pack();
	}

	private void initOthers() {
		
		// tabella dei paesi
		countriesTable = new DefaultTableModel(new String[] { "Paese", "N.Città" }, 0);
		tblElencoPaesi = new JTable();
		tblElencoPaesi.setModel(countriesTable);
		
		scrlElencoPaesi = new javax.swing.JScrollPane();
		scrlElencoPaesi.setViewportView(tblElencoPaesi);

		// text area log
		taLog = new JTextArea();
		taLog.setColumns(20);
		taLog.setRows(5);
		
		scrollableTa = new JScrollPane();
		scrollableTa.setViewportView(taLog);
		
		// panel
		
		pnlMappa = new JPanel();
		pnlMappa.setBorder(BorderFactory.createTitledBorder(""));
		pnlMappa.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				pnlMappaMouseClicked(evt);
			}
		});
		pnlMappa.setLayout(null);
		
		pnlMain = new JPanel();
		
		pnlPaesi = new javax.swing.JPanel();
		pnlPaesi.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
		
		pnlLog = new JPanel();
		pnlConfig = new JPanel();
		
		// tab
		
		tbMain = new JTabbedPane();
				
		tbMain.addTab("Paesi", null, pnlPaesi, "Mostra la mappa delle Città");
		tbMain.addTab("Log", null, pnlLog,"Visualizza il log della Simulazione");
		tbMain.addTab("Config", null, pnlConfig, "Imposta la Configurazione di Simulazione");
		
		// remote node

		lstNodi = new JList<String>();
		lstNodi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstNodi.setBorder(new LineBorder(new Color(0, 0, 0)));

		DefaultListModel<String> lstModel = new DefaultListModel<>();
		lstModel.addElement(Settings.MC_HOST);
		lstNodi.setModel(lstModel);
		
	}

	private void initSliders() {
		// config sliders

		sldTempo = new JSlider();
		sldTempo.setValue(Settings.TIME_SPEED);
		sldTempo.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				updateConfig();
			}
		});

		sldQuarantine = new JSlider();
		sldQuarantine.setValue(Settings.QUARANTENE_INFECTED_PERC);
		sldQuarantine.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblQuarantineValue.setText(sldQuarantine.getValue() + "%");
				updateConfig();
			}
		});

		sldBomb = new JSlider();
		sldBomb.setValue(Settings.BOMB_INFECTED_PERC);
		sldBomb.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblBombValue.setText(sldBomb.getValue() + "%");
				updateConfig();
			}
		});

		sldTaskForce = new JSlider();
		sldTaskForce.setValue(Settings.TASK_FORCE_INFECTED_PERC);
		sldTaskForce.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblTaskForceValue.setText( sldTaskForce.getValue() + "%");
				updateConfig();
			}
		});
	}

	private void initLabels() {
		// labels

		lblTaskForce = new JLabel("Task Force");
		lblTempo = new JLabel("Tempo");

		lblDay = new JLabel("0");
		lblDay.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
		lblDay.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		lblTotPersone = new JLabel();
		lblTotaleInfetti = new JLabel();
		lblTotaleSani = new JLabel();
		lblDiSeguito = new JLabel("Di seguito è possibile selezionare la percentuale di infezione minima per l'applicazione delle misure di emergenza");
		lblQuarantine = new JLabel("Quarantena");
		lblBomb = new JLabel("Sgancio Bomba");
		lblPersoneInViaggio = new JLabel("Persone in viaggio: 0");

		lblQuarantineValue = new JLabel(Settings.QUARANTENE_INFECTED_PERC + "");
		lblBombValue = new JLabel(Settings.BOMB_INFECTED_PERC + "");
		lblTaskForceValue = new JLabel(Settings.TASK_FORCE_INFECTED_PERC + "");
	}

	private void initButtons() {

		btnCreaPaese = new JToggleButton();
		btnCreaPaese.setSelected(false);
		btnCreaPaese.setText("Crea un paese");
		btnCreaPaese.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				btnCreaPaeseActionPerformed(evt);
			}
		});

		btnTucsonInspector = new JButton();
		btnTucsonInspector.setText("Mostra Tucson Inspector");
		btnTucsonInspector.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				btnTucsonInspectorActionPerformed(evt);
			}
		});

		btnPause = new JButton();
		btnPause.setEnabled(false);
		btnPause.setText("Pausa");
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				btnPauseActionPerformed(evt);
			}
		});

		btnAvvia = new JButton();
		btnAvvia.setText("Avvia Simulazione");
		btnAvvia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				btnAvviaActionPerformed(evt);
			}
		});

		btnFerma = new JButton();
		btnFerma.setEnabled(false);
		btnFerma.setText("Ferma Simulazione");
		btnFerma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				btnFermaActionPerformed(evt);
			}
		});

		btnPaziente0 = new JToggleButton();
		btnPaziente0.setText("Imposta paziente 0");
		btnPaziente0.setEnabled(false);

		btnRMA = new JButton();
		btnRMA.setText("Mostra RMA");
		btnRMA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				btnRMAActionPerformed(evt);
			}
		});

		btnAggiungiNodo = new JButton("Aggiungi Nodo");
		btnAggiungiNodo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAggiungiNodoActionPerformed(e);
			}
		});

	}

	private void initGroupsLayout(){
		
		JSeparator separator = new JSeparator();
		
		GroupLayout pnlPaesiLayout = new GroupLayout(pnlPaesi);
		pnlPaesiLayout.setHorizontalGroup(pnlPaesiLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(pnlPaesiLayout.createSequentialGroup().addContainerGap()
						.addGroup(pnlPaesiLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(pnlPaesiLayout.createSequentialGroup().addComponent(btnCreaPaese)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnPaziente0)
										.addGap(0, 751, Short.MAX_VALUE))
						.addComponent(pnlMappa, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(pnlPaesiLayout.createSequentialGroup()
								.addComponent(scrlElencoPaesi, GroupLayout.PREFERRED_SIZE, 532,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(pnlPaesiLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblPersoneInViaggio, GroupLayout.DEFAULT_SIZE, 217,
												Short.MAX_VALUE)
										.addComponent(lblTotPersone, GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE))
								.addGap(18)
								.addGroup(pnlPaesiLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(lblTotaleInfetti, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(lblTotaleSani, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE))))
				.addContainerGap()));
		pnlPaesiLayout.setVerticalGroup(pnlPaesiLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(pnlPaesiLayout.createSequentialGroup().addContainerGap()
						.addGroup(pnlPaesiLayout.createParallelGroup(Alignment.BASELINE).addComponent(btnCreaPaese)
								.addComponent(btnPaziente0))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(pnlMappa, GroupLayout.PREFERRED_SIZE, 454, GroupLayout.PREFERRED_SIZE).addGap(
								11)
				.addGroup(pnlPaesiLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrlElencoPaesi, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
						.addGroup(pnlPaesiLayout.createSequentialGroup()
								.addGroup(pnlPaesiLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblTotPersone).addComponent(lblTotaleInfetti,
												GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
								.addGap(17)
								.addGroup(pnlPaesiLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblTotaleSani).addComponent(lblPersoneInViaggio))))
				.addContainerGap()));
		pnlPaesi.setLayout(pnlPaesiLayout);
		
		
		GroupLayout pnlLogLayout = new GroupLayout(pnlLog);
		pnlLog.setLayout(pnlLogLayout);
		pnlLogLayout.setHorizontalGroup(pnlLogLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(scrollableTa, GroupLayout.DEFAULT_SIZE, 1090, Short.MAX_VALUE));
		pnlLogLayout.setVerticalGroup(pnlLogLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(scrollableTa, GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE));
		
		
		
		GroupLayout gl_pnlConfig = new GroupLayout(pnlConfig);
		gl_pnlConfig.setHorizontalGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlConfig.createSequentialGroup().addGap(26)
						.addGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING)
								.addComponent(lstNodi, GroupLayout.PREFERRED_SIZE, 641, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_pnlConfig.createSequentialGroup().addComponent(btnAggiungiNodo)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_pnlConfig.createSequentialGroup()
														.addComponent(sldQuarantine, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(lblQuarantineValue, GroupLayout.PREFERRED_SIZE,
																85, GroupLayout.PREFERRED_SIZE))
												.addComponent(separator, GroupLayout.PREFERRED_SIZE, 1,
														GroupLayout.PREFERRED_SIZE)
												.addGroup(gl_pnlConfig.createSequentialGroup()
														.addComponent(sldBomb, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(lblBombValue, GroupLayout.PREFERRED_SIZE, 85,
																GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_pnlConfig.createSequentialGroup()
														.addComponent(sldTaskForce, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(lblTaskForceValue, GroupLayout.PREFERRED_SIZE, 85,
																GroupLayout.PREFERRED_SIZE)))))
						.addGap(423))
				.addGroup(gl_pnlConfig.createSequentialGroup().addGap(29)
						.addGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlConfig.createSequentialGroup()
										.addComponent(lblTaskForce, GroupLayout.PREFERRED_SIZE, 108,
												GroupLayout.PREFERRED_SIZE)
										.addContainerGap())
						.addGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING).addComponent(lblBomb)
								.addGroup(gl_pnlConfig.createSequentialGroup()
										.addComponent(lblDiSeguito, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addContainerGap(337, Short.MAX_VALUE))
								.addGroup(gl_pnlConfig.createSequentialGroup().addComponent(lblQuarantine)
										.addGap(976))))));
		gl_pnlConfig.setVerticalGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlConfig.createSequentialGroup().addGap(29).addComponent(lblDiSeguito).addGap(17)
						.addGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING)
								.addComponent(lblQuarantine, Alignment.TRAILING)
								.addComponent(sldQuarantine, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuarantineValue, Alignment.TRAILING))
				.addGroup(gl_pnlConfig.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlConfig.createSequentialGroup().addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_pnlConfig.createParallelGroup(Alignment.TRAILING).addComponent(lblBomb)
										.addComponent(sldBomb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
						.addGroup(Alignment.TRAILING,
								gl_pnlConfig.createSequentialGroup().addGap(13).addComponent(lblBombValue)))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(gl_pnlConfig.createParallelGroup(Alignment.TRAILING).addComponent(lblTaskForce)
						.addComponent(sldTaskForce, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(lblTaskForceValue))
				.addGap(35)
				.addGroup(gl_pnlConfig.createParallelGroup(Alignment.TRAILING)
						.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAggiungiNodo)).addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(lstNodi, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(281, Short.MAX_VALUE)));
		pnlConfig.setLayout(gl_pnlConfig);

		GroupLayout gl_pnlMain = new GroupLayout(pnlMain);
		pnlMain.setLayout(gl_pnlMain);

		gl_pnlMain.setHorizontalGroup(gl_pnlMain.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(tbMain, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
				.addGroup(gl_pnlMain.createSequentialGroup().addContainerGap()
						.addGroup(gl_pnlMain.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(gl_pnlMain.createSequentialGroup().addComponent(btnAvvia)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(btnPause, GroupLayout.PREFERRED_SIZE, 146,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(btnRMA, GroupLayout.PREFERRED_SIZE, 272,
												GroupLayout.PREFERRED_SIZE)
										.addGap(18, 18, 18)
										.addComponent(btnTucsonInspector, GroupLayout.PREFERRED_SIZE, 283,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(btnFerma))
								.addGroup(gl_pnlMain.createSequentialGroup().addComponent(lblTempo).addGap(36, 36, 36)
										.addComponent(sldTempo, GroupLayout.PREFERRED_SIZE, 705,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(lblDay, GroupLayout.PREFERRED_SIZE, 131,
												GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));
		gl_pnlMain.setVerticalGroup(gl_pnlMain.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				gl_pnlMain.createSequentialGroup().addContainerGap()
						.addGroup(gl_pnlMain.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(gl_pnlMain.createSequentialGroup().addGap(10, 10, 10)
										.addComponent(sldTempo, GroupLayout.PREFERRED_SIZE, 32,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
								.addGroup(gl_pnlMain.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(GroupLayout.Alignment.TRAILING,
												gl_pnlMain.createSequentialGroup().addComponent(lblTempo).addGap(26, 26,
														26))
										.addGroup(gl_pnlMain.createSequentialGroup()
												.addComponent(lblDay, GroupLayout.PREFERRED_SIZE, 53,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))))
						.addComponent(tbMain, GroupLayout.PREFERRED_SIZE, 612,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(gl_pnlMain.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(btnAvvia).addComponent(btnFerma).addComponent(btnRMA)
								.addComponent(btnTucsonInspector).addComponent(btnPause))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		tbMain.getAccessibleContext().setAccessibleName("Log");

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(
				pnlMain, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(
				pnlMain, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
	}
	
	/* vars */

	private JButton btnAvvia;
	private JButton btnFerma;
	private JButton btnRMA;
	private JButton btnTucsonInspector;
	private JButton btnPause;
	private JButton btnAggiungiNodo;
	private JToggleButton btnPaziente0;
	private javax.swing.JPanel pnlMain;
	private javax.swing.JScrollPane scrollableTa;
	private javax.swing.JLabel lblDay;
	private javax.swing.JLabel lblTempo;
	private javax.swing.JPanel pnlLog;
	private javax.swing.JPanel pnlMappa;
	private javax.swing.JPanel pnlPaesi;
	private javax.swing.JScrollPane scrlElencoPaesi;
	private javax.swing.JSlider sldTempo;
	private javax.swing.JTextArea taLog;
	private javax.swing.JTabbedPane tbMain;
	private javax.swing.JTable tblElencoPaesi;
	private javax.swing.JToggleButton btnCreaPaese;
	private JPanel pnlConfig;
	private JLabel lblBomb;
	private JLabel lblDiSeguito;
	private JSlider sldQuarantine;
	private JSlider sldBomb;
	private JSlider sldTaskForce;
	private JLabel lblQuarantineValue;
	private JLabel lblBombValue;
	private JLabel lblTaskForceValue;
	private JLabel lblTotPersone;
	private JLabel lblTotaleInfetti;
	private JLabel lblTotaleSani;
	private JLabel lblPersoneInViaggio;
	private JLabel lblQuarantine;
	private JLabel lblTaskForce;
	private JList<String> lstNodi;
}
