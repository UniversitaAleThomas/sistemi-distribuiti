package it.unibo.sistemidistribuiti.zo.agent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import it.unibo.sistemidistribuiti.zo.agent.behaviors.HandleSimulationStateBehaviour;
import it.unibo.sistemidistribuiti.zo.agent.behaviors.TucsonSingleOperationAsyncBehaviour;
import it.unibo.sistemidistribuiti.zo.config.Settings;
import it.unibo.sistemidistribuiti.zo.dao.ConfigDAO;
import it.unibo.sistemidistribuiti.zo.utils.AgentConstants;
import it.unibo.sistemidistribuiti.zo.utils.TupleUtils;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;

/**
 * Carica le reaction e rimane in wait dell'eod
 * @author Ale
 */
public class TimerAgent extends BaseAgent {

	private static final long serialVersionUID = 2021703888848537967L;

	private static Logger log = LogManager.getLogger();

	private ParallelBehaviour mainBehaviour;

	private int day = 1, numeroCitta = 0;

	public void setup() {
		
		// 1. init tucson interaction
		super.setup();
		
		registerDFService(AgentConstants.TYPE_TIMER);
		
		numeroCitta = (int) getArguments()[1];
		
		// mainBehaviour
		mainBehaviour = new ParallelBehaviour();
		mainBehaviour.addSubBehaviour(new ControlBehaviourInternal(getTucsonTupleCentreId(), getBridge()));

		// Aggiunta del mainBehaviour all'agente
		addBehaviour(mainBehaviour);

		log.info("Fine del setup per " + getAID());
	}

	/* BEHAVIOUR */

	/**
	 * Behavior in ascolto dei messaggi di start del controller agent
	 */
	private class ControlBehaviourInternal extends HandleSimulationStateBehaviour {

		private static final long serialVersionUID = -1483443921427456972L;
		private SequentialBehaviour sb;
		
		int timespeed = Settings.TIME_SPEED;

		public ControlBehaviourInternal(TucsonTupleCentreId tucsonTupleCentreId, BridgeToTucson bridge) {
			super(tucsonTupleCentreId, bridge);
			sb = new SequentialBehaviour();
		}
		
		@Override
		public void onSimStart() {
			
//			cities = AgentSearchUtil.getAllCities(getAgent());
			try {
				/* Aggiungo i behavior della giornata */
				addDayBehaviours(sb);
				mainBehaviour.addSubBehaviour(sb);
			} catch (InvalidLogicTupleException e) {
				log.error(e);
			}
			
			log.info("Timer " + getAID().getLocalName() + " avviato.");
		}

		@Override
		public void onSimPause() {
			mainBehaviour.removeSubBehaviour(sb);
			log.info("Paused " + getAgent());
		}

		@Override
		public void onSimResume() {
			mainBehaviour.addSubBehaviour(sb);
			log.info("Resumed " + getAgent());
		}
		
		@Override
		public void onSimStop() {
			log.info("doDelete: " + getAgent());
			
			TimerAgent.this.doDelete();
		}
		
		private void addDayBehaviours(SequentialBehaviour sb) throws InvalidLogicTupleException {

			/* 1 - lancio lo start of day */
			sb.addSubBehaviour(new OneShotBehaviour() {
				private static final long serialVersionUID = 723642569427L;

				@Override
				public void action() {
					log.info("----------------------Iniziato giorno " + day );
					TupleUtils.out(TimerAgent.this, "sod(" + day + ")");
				}
			});

			/*
			 * 3 - leggo il tempo virtuale
			 */
			LogicTuple sTuple = LogicTuple.parse(ConfigDAO.TEMPLATE);
			
			final Rd rdTime = new Rd(getTucsonTupleCentreId(), sTuple);
			sb.addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(TimerAgent.this, getBridge(), rdTime, res -> {
				// la tupla è sempre presente nel tc.. la chiamata deve essere istantanea
				timespeed = res.getTuple().getArg("time_speed").getArg(0).intValue();
			}));

			/*
			 * 2 - aspetto che tutti abbiano eseguito la simulazione giornaliera
			 */
			
			log.info("[Timer] In attesa delle azioni di " + numeroCitta + " città");
			
			final In in = new In(getTucsonTupleCentreId(), LogicTuple.parse("executed_action(" + numeroCitta + ")"));
			sb.addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(TimerAgent.this, getBridge(), in, res -> {

				try {
					Thread.sleep((100 - timespeed) * 20); // 0-> 2s,
															// 100->live
				} catch (InterruptedException e) {
					log.error(e);
				}
			}));

			/* 3 - invio la tupla di fine giornata */
			sb.addSubBehaviour(new OneShotBehaviour() {

				private static final long serialVersionUID = 36772569427L;

				@Override
				public void action() {

					TupleUtils.out(TimerAgent.this, "eod(" + day + ")");

					log.info("----------------------Terminato giorno " + day );

					/* Incremento il giorno */
					day++;

					try {
						/* Ricominciamo */
						addDayBehaviours(sb);
					} catch (InvalidLogicTupleException e) {
						log.error(e);
					}
				}
			});
		}

	}

}