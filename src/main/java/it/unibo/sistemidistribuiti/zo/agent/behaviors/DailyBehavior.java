package it.unibo.sistemidistribuiti.zo.agent.behaviors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SequentialBehaviour;

public abstract class DailyBehavior extends SequentialBehaviour {
	
	private static final long serialVersionUID = 3406329720628328887L;

	private static Logger log = LogManager.getLogger();

	private TucsonTupleCentreId tci;
	private BridgeToTucson bridge;

	private int day;

	public DailyBehavior(TucsonTupleCentreId tucsonTupleCentreId, BridgeToTucson bridge) {
		day = 0;
		this.tci = tucsonTupleCentreId;
		this.bridge = bridge;
	}

	@Override
	public void onStart() {
		addDailyBehaviours();
		super.onStart();
	}

	public abstract Behaviour getDailyBehaviour();

	private void addDailyBehaviours() {
		try {
			LogicTuple SOD_TUPLE = LogicTuple.parse("sod(X)");

			// 1. Leggo la tupla di sod
			addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(getAgent(), bridge, new Rd(tci, SOD_TUPLE), res -> {
				day = res.getTuple().getArg(0).intValue();
				//log.info("#### SOD dalla citta " + getAgent() + " , day #" + day);

				// 3. Itero non appena finisce la giornata (eod)
				try {
					addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(getAgent(), bridge, new Rd(tci, LogicTuple.parse("eod("+day+")")), eodRes -> {
						//log.info("#### EOD dalla citta " + getAgent() + " , day #" + day);
						addDailyBehaviours();
					}));
				} catch (InvalidLogicTupleException e) {
					e.printStackTrace();
				}
				//log.info("#### Aggiunti DailyBehaviours all'agente: " + getAgent());
				
			}));
			
			// 2. Eseguo il daily behavior
			addSubBehaviour(getDailyBehaviour());
			
		} catch (InvalidLogicTupleException e) {
			log.error(e);
		}
	}

	public int getCurrentDay() {
		return day;
	}

}
