package it.unibo.sistemidistribuiti.zo.agent.behaviors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.asynchSupport.actions.ordinary.Rd;
import it.unibo.sistemidistribuiti.zo.utils.SimulationConstants;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import jade.core.behaviours.ParallelBehaviour;

public abstract class HandleSimulationStateBehaviour extends ParallelBehaviour {
	
	private static final long serialVersionUID = 256250995982924622L;

	private static Logger log = LogManager.getLogger();
	
	private TucsonTupleCentreId tci;
	private BridgeToTucson bridge;
	
	public HandleSimulationStateBehaviour(TucsonTupleCentreId tucsonTupleCentreId, BridgeToTucson bridge) {
		this.tci = tucsonTupleCentreId;
		this.bridge = bridge;
	}
	
	@Override
	public void onStart() {
		addControlBehaviours();
		super.onStart();
	}

	/**
	 * Aggiungiamo il behavior di start
	 */
	private void addControlBehaviours(){
		
		// NOTA: la start e stop sono gestiti come behav oneshot
		// pause e resume in modo ciclico
		
		try {
			
			//behavior di start
			
			Rd rdStart = new Rd(tci, LogicTuple.parse("simulation("+ SimulationConstants.START +")"));
			addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(getAgent(), bridge, rdStart, res -> {
				log.info("Letta tupla di start: " + getAgent());
				
				/* Hook */
				onSimStart();
			}));
			
			//behavior di stop
			
			Rd rdStop = new Rd(tci,  LogicTuple.parse("simulation("+ SimulationConstants.STOP +")"));
			addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(getAgent(), bridge, rdStop, res -> {
				log.info("Letta tupla di stop: " + getAgent());
				
				/* Hook */
				onSimStop();
			}));
			
			// behaviour di pausa
			addCheckPauseBehaviours();
			
			// behaviour di resume
			addCheckResumeBehaviours();
			
		} catch (InvalidLogicTupleException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Aggiunge al parallel behaviour un sub-behav per il controllo del messaggio di resume
	 */
	private void addCheckResumeBehaviours(){
		try {
			Rd rdResume = new Rd(tci, LogicTuple.parse("simulation("+ SimulationConstants.RESUME +")"));
			
			addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(getAgent(), bridge, rdResume, res -> {
				
				log.info("Letta tupla di resume: " + getAgent());
				
				/* Hook */
				onSimResume();
				
				/* Riapplico il behav di pausa */
				addCheckPauseBehaviours();
			}));
			
		} catch (InvalidLogicTupleException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Aggiunge al parallel behaviour un sub-behav per il controllo del messaggio di pausa
	 */
	private void addCheckPauseBehaviours(){
		try {
			Rd rdPause = new Rd(tci, LogicTuple.parse("simulation("+ SimulationConstants.PAUSE +")"));
			
			addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(getAgent(), bridge, rdPause, res -> {
				
				log.info("Letta tupla di pausa: " + getAgent());
				
				/* Hook */
				onSimPause();
				
				/* Riapplico il behav di start */
				addCheckResumeBehaviours();
			}));
			
		} catch (InvalidLogicTupleException e) {
			e.printStackTrace();
		}
	}
	
	
	public abstract void onSimStart();

	public abstract void onSimPause();
	
	public abstract void onSimStop();

	public abstract void onSimResume();
}
