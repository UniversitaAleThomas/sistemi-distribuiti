package it.unibo.sistemidistribuiti.zo.agent.behaviors;

import alice.tucson.asynchSupport.actions.AbstractTucsonAction;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.tucson.jade.coordination.IAsynchCompletionBehaviour;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;

public class TucsonSingleOperationAsyncBehaviour extends Behaviour implements IAsynchCompletionBehaviour {
	
	private static final long serialVersionUID = -761171526516270770L;

	//private static Logger log = LogManager.getLogger();

	public interface TucsonSingleOperationResults {
		void responseReceived(TucsonOpCompletionEvent res);
	}

	private TucsonSingleOperationResults tucsonSingleOperationResults;

	private BridgeToTucson bridge;

	private AbstractTucsonAction action;
	private Agent agent;

	public TucsonSingleOperationAsyncBehaviour(Agent agent, BridgeToTucson bridge, AbstractTucsonAction action) {
		this(agent, bridge, action, null);
	}

	public TucsonSingleOperationAsyncBehaviour(Agent agent, BridgeToTucson bridge, AbstractTucsonAction action,
			TucsonSingleOperationResults tucsonSingleOperationResults) {
		this.tucsonSingleOperationResults = tucsonSingleOperationResults;
		this.bridge = bridge;
		this.action = action;
		this.agent = agent;

	}

	@Override
	public void onStart() {
		super.onStart();
		bridge.asynchronousInvocation(action, this, agent);
//		log.info("Async invocation chiamato per: " + agent + " op: " + action.toString());
	}

	@Override
	public void action() {
	}

	@Override
	public boolean done() {
		return completed;
	}

	private boolean completed = false;

	@Override
	public void setTucsonOpCompletionEvent(TucsonOpCompletionEvent tce) {

		if (tucsonSingleOperationResults != null) {
			// se il risultato va gestito..
			tucsonSingleOperationResults.responseReceived(tce);
		}
		
		completed = true;
	}

}