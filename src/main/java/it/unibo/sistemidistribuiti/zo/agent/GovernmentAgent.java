package it.unibo.sistemidistribuiti.zo.agent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.sistemidistribuiti.zo.utils.SimulationConstants;
import it.unibo.sistemidistribuiti.zo.utils.TupleUtils;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;

/**
 * Agente in grado di controllare lo start e lo stop di tutti gli agenti del
 * programma
 */
public class GovernmentAgent extends BaseAgent {

	private static final long serialVersionUID = 4021293695132454386L;

	private static Logger log = LogManager.getLogger();

	@Override
	protected void setup() {
		
		// 1. init tucson interaction
		super.setup();
		
		// 2. Aggiunta behaviour di rescue city
		ParallelBehaviour mainBehaviour = new ParallelBehaviour();
		mainBehaviour.addSubBehaviour(new StartSimulationBehavior());
		addBehaviour(mainBehaviour);
		
		log.info("Fine del setup per " + getAID());
	}
	
	@Override
	public void doSuspend() {
		log.info("Richiesta la sospensione di " + getAID());
		
		// inviamo la pausa della simulazione
		TupleUtils.out(GovernmentAgent.this, "simulation("+ SimulationConstants.PAUSE +")");
		
		super.doSuspend();
	}
	
	@Override
	public void doActivate() {
		
		super.doActivate();
		
		log.info("Richiesta di resume per " + getAID());
		
		// inviamo il resume della simulazione
		TupleUtils.out(GovernmentAgent.this, "simulation("+ SimulationConstants.RESUME +")");
	}
	
	@Override
	public void doDelete() {
		log.info("Richiesta di stop per " + getAID());
		
		TupleUtils.out(GovernmentAgent.this, "simulation("+ SimulationConstants.STOP +")");
		
		super.doDelete();
	}
	
	/* BEHAVIOR */
	
	/*
	 * Behavior per l'invio del messaggio di start
	 */
	private class StartSimulationBehavior extends OneShotBehaviour {

		private static final long serialVersionUID = 6651758844122704767L;

		@Override
		public void action() {
			TupleUtils.out(GovernmentAgent.this, "simulation("+ SimulationConstants.START +")");
			log.info("Inviato messaggio di start simulation: " + getAgent());
		}
		
	}
}
