package it.unibo.sistemidistribuiti.zo.agent;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.tucson.asynchSupport.actions.specification.SetS;
import alice.tucson.utilities.Utils;
import it.unibo.sistemidistribuiti.zo.config.Settings;
import it.unibo.sistemidistribuiti.zo.dao.ConfigDAO;
import it.unibo.sistemidistribuiti.zo.event.EventDispatcher;
import it.unibo.sistemidistribuiti.zo.event.configchange.ConfigChangeEvent;
import it.unibo.sistemidistribuiti.zo.event.configchange.ConfigChangeEventHandler;
import it.unibo.sistemidistribuiti.zo.utils.TupleUtils;
import jade.core.ServiceException;

/**
 * Agente in grado di controllare lo start e lo stop di tutti gli agenti del
 * programma
 */
public class SettingsAgent extends BaseAgent {

	private static final long serialVersionUID = 4021293695132454386L;

	private static Logger log = LogManager.getLogger();

	@Override
	protected void setup() {
		
		// 1. init tucson interaction
		super.setup();
		
		// 2. register as config change handler
		handleConfigChange();
		
		log.info("Registered to Event Dispatcher");
		
		// 3. load reactions
		loadReaction();
		
		log.info("Reactions loaded successfully");
		
		// 4. load simulator default settings
		loadDefaultSettings();
		
		log.info("Default config loaded successfully");
	}
	
	/**
	 * Si rigistra sull'event dispatcher per gestire il cambio di config
	 */
	private void handleConfigChange(){
		
		EventDispatcher.get().addEventHandler(new ConfigChangeEventHandler() {
			@Override
			public void run(ConfigChangeEvent cce) {
				TupleUtils.out(SettingsAgent.this, cce.getConfig());
			}
		});
		
	}
	
	/**
	 * Carica le configurazioni di default
	 */
	private void loadDefaultSettings(){
		TupleUtils.out(SettingsAgent.this, new ConfigDAO(Settings.TIME_SPEED, Settings.QUARANTENE_INFECTED_PERC, Settings.TASK_FORCE_INFECTED_PERC, Settings.BOMB_INFECTED_PERC));
	}
	
	/**
	 * Carica le reaction della simulazione
	 */
	private void loadReaction(){

		try {
			final LogicTuple specT = new LogicTuple("spec", new Value(Utils.fileToString(Settings.GOVERNMENT_REACTION_FILE)));
			SetS op = new SetS(getTucsonTupleCentreId(), specT);
			SettingsAgent.this.getBridge().asynchronousInvocation(op);
		} catch (ServiceException | IOException e) {
			e.printStackTrace();
		}
	}

}
