package it.unibo.sistemidistribuiti.zo.agent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.asynchSupport.actions.AbstractTucsonAction;
import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.bulk.RdAll;
import it.unibo.sistemidistribuiti.zo.agent.behaviors.DailyBehavior;
import it.unibo.sistemidistribuiti.zo.agent.behaviors.HandleSimulationStateBehaviour;
import it.unibo.sistemidistribuiti.zo.agent.behaviors.TucsonSingleOperationAsyncBehaviour;
import it.unibo.sistemidistribuiti.zo.algorithm.Dead;
import it.unibo.sistemidistribuiti.zo.algorithm.DeadImpl;
import it.unibo.sistemidistribuiti.zo.algorithm.Infection;
import it.unibo.sistemidistribuiti.zo.algorithm.InfectionImpl;
import it.unibo.sistemidistribuiti.zo.algorithm.Migration;
import it.unibo.sistemidistribuiti.zo.algorithm.MigrationImpl;
import it.unibo.sistemidistribuiti.zo.algorithm.TaskForceAction;
import it.unibo.sistemidistribuiti.zo.algorithm.TaskForceActionImpl;
import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.dao.DailyInfoDAO;
import it.unibo.sistemidistribuiti.zo.dao.MigrationDAO;
import it.unibo.sistemidistribuiti.zo.utils.AclMessageFactory;
import it.unibo.sistemidistribuiti.zo.utils.AgentConstants;
import it.unibo.sistemidistribuiti.zo.utils.AgentSearchUtil;
import it.unibo.sistemidistribuiti.zo.utils.MSG;
import it.unibo.sistemidistribuiti.zo.utils.MessageTemplateFactory;
import it.unibo.sistemidistribuiti.zo.utils.TupleUtils;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class CityAgent extends BaseAgent {

	private static final long serialVersionUID = -6801637415393412262L;
	private static Logger log = LogManager.getLogger();
	private ParallelBehaviour mainBehaviour;

	private CityDAO ci;
	private List<CityDAO> cities;

	private Infection infection;
	private Migration migration;
	private Dead dead;
	private TaskForceAction taskForceAction;

	private DailyBehavior dailyBehaviour;

	@Override
	protected void setup() {

		// 1. init tucson interaction
		super.setup();

		ci = (CityDAO) getArguments()[1]; // arg 0 -> city info

		registerDFService(AgentConstants.TYPE_CITY, AgentConstants.TYPE_CITY + "_" + ci.getId());
		
		infection = new InfectionImpl();
		migration = new MigrationImpl();
		dead = new DeadImpl();
		taskForceAction = new TaskForceActionImpl();

		// Creazione del mainBehaviour parallelo e aggiunta dei subBehaviour al mainBehaviour
		mainBehaviour = new ParallelBehaviour();
		mainBehaviour.addSubBehaviour(new StartBehaviour(getTucsonTupleCentreId(), getBridge()));

		// Aggiunta del mainBehaviour all'agente citta
		addBehaviour(mainBehaviour);

		TupleUtils.out(this, ci);

		log.info("Fine del setup per " + getAID());
	}

	/* BEHAVIOURS */
	
	private class CityDailyBehaviour extends DailyBehavior {

		private static final long serialVersionUID = -7956886362429095457L;

		public CityDailyBehaviour() {
			super(getTucsonTupleCentreId(), getBridge());
		}

		@Override
		public Behaviour getDailyBehaviour() {

			SequentialBehaviour mb = new SequentialBehaviour();
			if (!ci.isNuclearized()) {
				LogicTuple tuple;
				try {
					tuple = LogicTuple.parse(CityDAO.TEMPLATE);
					mb.addSubBehaviour(new TucsonSingleOperationAsyncBehaviour(getAgent(), getBridge(),
							new RdAll(getTucsonTupleCentreId(), tuple), res -> {
								List<CityDAO> results = new ArrayList<CityDAO>();
								List<LogicTuple> resList = res.getTupleList();
								for (LogicTuple t : resList) {
									results.add(new CityDAO(t));
								}
								cities = results;
							}));

				} catch (InvalidLogicTupleException e) {
					log.error(e);
				}

				mb.addSubBehaviour(new MovementBehaviour());
				mb.addSubBehaviour(new InfectionBehaviour());
				mb.addSubBehaviour(new DeadBehaviour());
				if (ci.taskForce) {
					mb.addSubBehaviour(new TaskForceActionBehaviour());
				}
			}
			mb.addSubBehaviour(new PublishDataBehaviour());

			return mb;
		}
	}
	
	int peopleSent, peopleReceived, newInfected, infectedDeaths, healthyDeaths, tfDeaths;
	
	/**
	 * Behavior che implementa la gestione delle partenze/arrivi degli abitanti
	 */
	private class MovementBehaviour extends OneShotBehaviour {

		private static final long serialVersionUID = 3254380807813142102L;
		
		@Override
		public void action() {

			DailyInfoDAO dailyInfo = new DailyInfoDAO(dailyBehaviour.getCurrentDay(), cities);
			List<MigrationDAO> migrationList = migration.apply(ci, dailyInfo);
			
			peopleSent = 0;
			peopleReceived = 0;
			
			for (MigrationDAO mig : migrationList) {

				try {

					ACLMessage msg = AclMessageFactory.getAclMessage(MSG.START_TRANSFERT, dailyInfo.getDay() + mig.getTripDuration());
					DFAgentDescription dstAgent = AgentSearchUtil.getCity(CityAgent.this, mig.getDestination());
					msg.addReceiver(dstAgent.getName());
					msg.setContentObject(mig);

					send(msg);
					peopleSent += mig.getTotal();

				} catch (IOException e) {
					log.error("Errore durante l'invio del messaggio di migrazione: " + e.getMessage());
				}

				ci.setPopulation(ci.getPopulation() - mig.getTotal());
				ci.setInfected(ci.getInfected() - mig.getInfected());
			}

			// Controlliamo eventuali arrivi giornalieri
			MessageTemplate mt = MessageTemplateFactory.getMessageTemplate(MSG.START_TRANSFERT, dailyInfo.getDay());

			ACLMessage msg = CityAgent.this.receive(mt);

			while (msg != null) {

				try {
					// log.info("Città pre migranti: " + ci);

					MigrationDAO mig = (MigrationDAO) msg.getContentObject();
					ci.setPopulation(ci.getPopulation() + mig.getTotal());
					ci.setInfected(ci.getInfected() + mig.getInfected());
					
					peopleReceived += mig.getTotal();

				} catch (UnreadableException e) {
					log.error("Errore durante la ricezione del messaggio di migrazione: " + e.getMessage());
				}
				msg = CityAgent.this.receive(mt);
			}
			
			log.info("["+ci.getName()+"] Migranti inviati: " + peopleSent + ", ricevuti: " + peopleReceived);
		}
	}

	/**
	 * Behavior che implementa la diffusione del contagio
	 */
	private class InfectionBehaviour extends OneShotBehaviour {
		private static final long serialVersionUID = -8012644269071576012L;

		@Override
		public void action() {
			
			newInfected = 0;
			
			int infectedBefore = ci.getInfected();
			
			// diffusione dell'infezione
			ci = infection.apply(ci, dailyBehaviour.getCurrentDay());
			
			newInfected = ci.getInfected() - infectedBefore;
			
			log.info("["+ci.getName()+"] Gli zombie hanno infettato " + newInfected + " persone");
		}
	}

	/**
	 * Behavior che implementa la morte naturale dei cittadini (infetti e non)
	 */
	private class DeadBehaviour extends OneShotBehaviour {
		private static final long serialVersionUID = -8012644269071576012L;

		@Override
		public void action() {
			
			healthyDeaths = 0;
			infectedDeaths = 0;
			
			int healthyBefore = ci.getHealthy();
			int infectedBefore = ci.getInfected();
			
			ci = dead.apply(ci, dailyBehaviour.getCurrentDay());
			
			healthyDeaths = healthyBefore - ci.getHealthy();
			infectedDeaths = infectedBefore-ci.getInfected();
			
			log.info("["+ci.getName()+"] Morte di " + healthyDeaths + " persone sane e " + infectedDeaths + " infetti");
		}
	}

	/**
	 * Behavior che implementa l'applicazione della task force
	 */
	private class TaskForceActionBehaviour extends OneShotBehaviour {
		private static final long serialVersionUID = -8012644269071576012L;

		@Override
		public void action() {
			
			tfDeaths = 0;
			
			int infectedBefore = ci.getInfected();
			
			ci = taskForceAction.apply(ci, dailyBehaviour.getCurrentDay());
			
			tfDeaths = infectedBefore - ci.getInfected();
			
			log.info("[Task force] Eliminati " + tfDeaths + " infetti dalla città " + ci.getName());
		}
	}

	/**
	 * Behavior che gestisce la pubblicazione delle info della città
	 */
	private class PublishDataBehaviour extends OneShotBehaviour {
		private static final long serialVersionUID = -8012644269071576012L;

		@Override
		public void action() {
			// 1. Pubblicazione dei dati della città
			TupleUtils.out(CityAgent.this, ci);
		
			// 2.Pubblicazione dei dati sulle partenze
			TupleUtils.out(CityAgent.this, "departures("+ peopleSent +")");
			
			// 3.Pubblicazione dei dati sugli arrivi
			TupleUtils.out(CityAgent.this, "arrivals("+ peopleReceived +")");
			
			peopleSent = 0;
			peopleReceived = 0;
			
			//log.info("### Pubblicate CI: " + ci.toString());
		}
	}

	/**
	 * Estensione del ControlBehavior
	 */
	private class StartBehaviour extends HandleSimulationStateBehaviour {

		private static final long serialVersionUID = 8541176658566841934L;

		public StartBehaviour(TucsonTupleCentreId tucsonTupleCentreId, BridgeToTucson bridge) {
			super(tucsonTupleCentreId, bridge);
		}

		@Override
		public void onSimStart() {
			dailyBehaviour = new CityDailyBehaviour();
			mainBehaviour.addSubBehaviour(dailyBehaviour);
			mainBehaviour.addSubBehaviour(getQuarantineSolutionBehaviour());
			mainBehaviour.addSubBehaviour(getTaskForceSolutionBehaviour());
			mainBehaviour.addSubBehaviour(getAtomicSolutionBehaviour());
		}

		// -- TASK FORCE
		
		private Behaviour getTaskForceSolutionBehaviour() {
			try {
				LogicTuple abTuple = LogicTuple.parse("task_force(" + ci.getId() + ")");
				AbstractTucsonAction inAtomicBomb = new In(getTucsonTupleCentreId(), abTuple);
				return new TucsonSingleOperationAsyncBehaviour(CityAgent.this, getBridge(), inAtomicBomb, res -> {
					ci.addTaskForce();
					mainBehaviour.addSubBehaviour(getStopTaskForceSolutionBehaviour());
				});
			} catch (InvalidLogicTupleException e) {
				e.printStackTrace();
			}
			return null;
		}

		private Behaviour getStopTaskForceSolutionBehaviour() {
			try {
				LogicTuple abTuple = LogicTuple.parse("stop_task_force(" + ci.getId() + ")");
				AbstractTucsonAction inAtomicBomb = new In(getTucsonTupleCentreId(), abTuple);
				return new TucsonSingleOperationAsyncBehaviour(CityAgent.this, getBridge(), inAtomicBomb, res -> {
					ci.removeTaskForce();
					mainBehaviour.addSubBehaviour(getTaskForceSolutionBehaviour());
				});
			} catch (InvalidLogicTupleException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		// -- QUARANTENA
		
		private Behaviour getQuarantineSolutionBehaviour() {
			try {
				LogicTuple abTuple = LogicTuple.parse("quarantine(" + ci.getId() + ")");
				AbstractTucsonAction inAtomicBomb = new In(getTucsonTupleCentreId(), abTuple);
				return new TucsonSingleOperationAsyncBehaviour(CityAgent.this, getBridge(), inAtomicBomb, res -> {
					ci.quarantinize();
					mainBehaviour.addSubBehaviour(getStopQuarantineSolutionBehaviour());
				});
			} catch (InvalidLogicTupleException e) {
				e.printStackTrace();
			}
			return null;
		}

		private Behaviour getStopQuarantineSolutionBehaviour() {
			try {
				LogicTuple abTuple = LogicTuple.parse("stop_quarantine(" + ci.getId() + ")");
				AbstractTucsonAction inAtomicBomb = new In(getTucsonTupleCentreId(), abTuple);
				return new TucsonSingleOperationAsyncBehaviour(CityAgent.this, getBridge(), inAtomicBomb, res -> {
					ci.dequarantinize();
					mainBehaviour.addSubBehaviour(getQuarantineSolutionBehaviour());
				});
			} catch (InvalidLogicTupleException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		// -- ATOMICA
		
		private Behaviour getAtomicSolutionBehaviour() {

			try {
				LogicTuple abTuple = LogicTuple.parse("atomic_bomb(" + ci.getId() + ")");
				AbstractTucsonAction inAtomicBomb = new In(getTucsonTupleCentreId(), abTuple);
				return new TucsonSingleOperationAsyncBehaviour(CityAgent.this, getBridge(), inAtomicBomb, res -> {
					ci.nuclearize();
				});
			} catch (InvalidLogicTupleException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public void onSimPause() {
			/* Nothing to do */
		}

		@Override
		public void onSimResume() {
			/* Nothing to do */
		}

		@Override
		public void onSimStop() {
			log.info("doDelete: " + getAgent());

			CityAgent.this.doDelete();
		}

	}

}
