package it.unibo.sistemidistribuiti.zo.agent;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.sistemidistribuiti.zo.config.Settings;
import it.unibo.sistemidistribuiti.zo.core.ZoAgentContainer;
import it.unibo.sistemidistribuiti.zo.core.ZoAgentContainerWrapper;
import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.utils.AgentConstants;
import jade.core.ContainerID;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.domain.introspection.AMSSubscriber;
import jade.domain.introspection.AddedContainer;
import jade.domain.introspection.Event;
import jade.domain.introspection.IntrospectionVocabulary;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

/**
 * Agente in grado di controllare lo start e lo stop di tutti gli agenti del
 * programma
 */
public class CityLoaderAgent extends BaseAgent {

	private static final long serialVersionUID = 4021293695132454386L;

	private static Logger log = LogManager.getLogger();

	private ParallelBehaviour mainBehaviour;

	private Map<String, List<CityDAO>> countriesMap;
	private List<InetSocketAddress> listaNodi;
	private String tname;
	private AgentController rca;
	private Map<String, ZoAgentContainer> zoAgentContainerMap;
	private ILoader loading;

	private int progCitta = 0;
	private int paesiIstanziati = 0;

	
	@Override
	protected void setup() {
		
		super.setup();
		
		registerDFService(AgentConstants.TYPE_CITY_LOADER);
		
		// Recupero args di lavoro
		getArgs();
		
		zoAgentContainerMap = new HashMap<String, ZoAgentContainer>();

		mainBehaviour = new ParallelBehaviour();
		mainBehaviour.addSubBehaviour(new InitContianerBehaviour());
		mainBehaviour.addSubBehaviour(new ContainerAMSSubscriber());
		mainBehaviour.addSubBehaviour(new loadingCompleteBehaviour());

		addBehaviour(mainBehaviour);
	}
	
	@SuppressWarnings("unchecked")
	private void getArgs() {
		tname = (String) getArguments()[0];
		countriesMap = (Map<String, List<CityDAO>>) getArguments()[1];
		listaNodi = (List<InetSocketAddress>) getArguments()[2];
		rca = (AgentController) getArguments()[3];
		loading = (ILoader) getArguments()[4];
	}

	private class InitContianerBehaviour extends OneShotBehaviour {
		private static final long serialVersionUID = -7187202262192116644L;

		@Override
		public void action() {
			int progPaese = 0;

			for (Map.Entry<String, List<CityDAO>> entrySet : countriesMap.entrySet()) {
				String nomePaese = entrySet.getKey();

				// scelgo i nodi disponibili via round robin
				InetSocketAddress currentNode = listaNodi.get(progPaese % listaNodi.size());

				ZoAgentContainer zoAgentContainer = null;
				if (Settings.MC_HOST.equals(currentNode.getHostString())) {
					// il subContainer è richiesto sullo stesso nodo del main container

					Profile paeseProfile = new ProfileImpl(false);
					paeseProfile.setParameter(Profile.CONTAINER_NAME, nomePaese);
					paeseProfile.setParameter(Profile.SERVICES, "it.unibo.tucson.jade.service.TucsonService");
					zoAgentContainer = ZoAgentContainerWrapper.instance(paeseProfile);
				} else {
					zoAgentContainer = ZoAgentContainerWrapper.instance(currentNode.getHostString(),
							currentNode.getPort(), nomePaese, rca);

				}
				zoAgentContainerMap.put(nomePaese, zoAgentContainer);
				progPaese++;
			}

		}

	}

	private class ContainerAMSSubscriber extends AMSSubscriber {
		private static final long serialVersionUID = 6650863245117821007L;

		@SuppressWarnings({ "rawtypes", "unchecked" })
		protected void installHandlers(Map handlers) {

			/**
			 * Handler di aggiunta container
			 */
			EventHandler addedHandler = new EventHandler() {

				private static final long serialVersionUID = 5956377660399271493L;

				public void handle(Event event) {
					AddedContainer addedContainer = (AddedContainer) event;

					String nomePaese = addedContainer.getContainer().getName();
					ContainerID cId = addedContainer.getContainer();

					List<CityDAO> citiesInfo = countriesMap.get(nomePaese);
					if(citiesInfo == null){
						// non è un container di tipo paese, probabilmente il Main-Container
						// nothing to do;
						return;
					}else {
						for (CityDAO ci : citiesInfo) {
							progCitta++;
							try {
								zoAgentContainerMap.get(nomePaese).createAgent("city_" + progCitta,
										CityAgent.class.getName(), cId,
										new Object[] { tname, ci });
								log.info("Lancia richiesta di creazione agente città: " + ci.getName());
							} catch (StaleProxyException e) {
								log.error(e);
							}

						}
						//tengo traccia del numero di container-paesi istanziati
						paesiIstanziati++;
					}
				}
			};
			handlers.put(IntrospectionVocabulary.ADDEDCONTAINER, addedHandler);
		}
	}

	private class loadingCompleteBehaviour extends CyclicBehaviour {
		private static final long serialVersionUID = -351046246668120323L;

		@Override
		public void action() {
			// Finisco quando il numero di container istanziati è uguale a quello di lavoro
			if (countriesMap.size() == paesiIstanziati) {
				loading.onComplete(progCitta);
				CityLoaderAgent.this.removeBehaviour(mainBehaviour);
				CityLoaderAgent.this.destroyDFService();
				CityLoaderAgent.this.doDelete();
			}
		}
	}

	public interface ILoader {
		void onComplete(int numeroCitta);
	}
}
