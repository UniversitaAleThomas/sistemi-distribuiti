package it.unibo.sistemidistribuiti.zo.agent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import it.unibo.sistemidistribuiti.zo.utils.AgentConstants;
import it.unibo.tucson.jade.exceptions.CannotAcquireACCException;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import it.unibo.tucson.jade.service.TucsonHelper;
import it.unibo.tucson.jade.service.TucsonService;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public abstract class BaseAgent extends Agent {
	private static final long serialVersionUID = 1405882603898476866L;
	private static Logger log = LogManager.getLogger();

	@Override
	protected void setup() {
		super.setup();
		bootstrapTucsonInteraction();
	}

	@Override
	protected void takeDown() {
//		destroyDFService();
		super.takeDown();
	}

	protected void registerDFService(String serviceType) {
		registerDFService(serviceType, null);
	}
	
	protected void registerDFService(String serviceType, String owner) {

		try {
			DFAgentDescription dfd = new DFAgentDescription();
			dfd.setName(getAID());

			ServiceDescription sd = new ServiceDescription();
			sd.setType(serviceType);
			sd.setName(AgentConstants.SERVICE_ZOMBIE);
			if(owner != null) {
				sd.setOwnership(owner);
			}
			dfd.addServices(sd);

			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	protected void destroyDFService() {

		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	/* -- Tucson Interaction -- */

	private BridgeToTucson bridge;
	private TucsonHelper helper;
	/*
	 * ID of tuple centre used for objective coordination
	 */
	private TucsonTupleCentreId tcid;

	/**
	 * Abilita la comunicazione con tucson
	 */
	protected void bootstrapTucsonInteraction() {
		try {
			
			/*
			 * build the tuple centre id
			 */
			tcid = new TucsonTupleCentreId(getTucsonNode());
			
			/*
			 * First of all, get the helper for the service you want to exploit
			 */
			this.helper = (TucsonHelper) this.getHelper(TucsonService.NAME);

			/*
			 * Obtain ACC (which is actually given to the bridge, not directly
			 * to your agent)
			 */
			this.helper.acquireACC(this, tcid.getNode(), tcid.getPort());
			
			/*
			 * Get the univocal bridge for the agent. Now, mandatory, set-up
			 * actions have been carried out and you are ready to coordinate
			 */
			this.bridge = this.helper.getBridgeToTucson(this);

		} catch (final ServiceException e) {
			log.error(
					">>> No TuCSoN service active, reboot JADE with -services it.unibo.tucson.jade.service.TucsonService option <<<");
			this.doDelete();
		} catch (final TucsonInvalidAgentIdException e) {
			log.error(
					">>> TuCSoN Agent ids should be compliant with Prolog sytnax (start with lowercase letter, no special symbols), choose another agent id <<<");
			this.doDelete();
		} catch (final TucsonInvalidTupleCentreIdException e) {
			// should not happen
			e.printStackTrace();
			this.doDelete();
		} catch (final CannotAcquireACCException e) {
			// should not happen
			e.printStackTrace();
			this.doDelete();
		}
	}

	public BridgeToTucson getBridge() {
		return bridge;
	}

	protected String getTucsonNode() {
		return (String) getArguments()[0];
	}

	public TucsonTupleCentreId getTucsonTupleCentreId() {
		return tcid;
	}
}
