package it.unibo.sistemidistribuiti.zo.algorithm;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;

public interface Dead {

	CityDAO apply(CityDAO ci, int currentDay);

}
