package it.unibo.sistemidistribuiti.zo.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.dao.DailyInfoDAO;
import it.unibo.sistemidistribuiti.zo.dao.MigrationDAO;

public class MigrationImpl implements Migration {
	private Random rand = new Random();
	private static double speed = 100;// velocità in unità di spazio per giorno
	private static double migrationFactorMin = 0.001;
	private static double migrationFactorMax = 0.02;

	@Override
	public List<MigrationDAO> apply(CityDAO start, DailyInfoDAO dailyInfo) {
		List<MigrationDAO> ret = new ArrayList<>();
		if (start == null || dailyInfo == null || dailyInfo.getCitiesInfo() == null
				|| start.getState() == CityDAO.QUARANTINE) {
			return ret;
		}
		for (CityDAO dst : dailyInfo.getCitiesInfo()) {
			if (start.getCompleteName().equals(dst.getCompleteName()) || dst.getState() == CityDAO.QUARANTINE) {
				continue;
			}
			double distance = computeDistance(start, dst);
			int healty = start.getPopulation() - start.getInfected();
			int infected = start.getInfected();

			double percMigr = migrationFactorMin + (migrationFactorMax - migrationFactorMin) * rand.nextDouble();

			healty *= percMigr;
			infected *= percMigr/10;
			int tripDuration = (int) Math.floor(distance / speed);
			if (tripDuration == 0)
				tripDuration = 1;
			ret.add(new MigrationDAO(healty, infected, tripDuration, start, dst));
		}

		return ret;
	}

	protected double computeDistance(CityDAO c1, CityDAO c2) {
		if (c1.getCoordinate() == null || c2.getCoordinate() == null) {
			return 0;
		}
		return c1.getCoordinate().distance(c2.getCoordinate());
	}
}
