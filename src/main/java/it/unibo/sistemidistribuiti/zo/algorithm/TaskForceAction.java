package it.unibo.sistemidistribuiti.zo.algorithm;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;

public interface TaskForceAction {

	CityDAO apply(CityDAO ci, int currentDay);

}
