package it.unibo.sistemidistribuiti.zo.algorithm;

import java.util.Random;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;

/**
 * Implementazione del tasso di mortalità naturale
 */
public class DeadImpl implements Dead {
	
	private static double deadFactorMin = 1D / 10000D;
	private static double deadFactorMax = 5D / 10000D;

	@Override
	public CityDAO apply(CityDAO ci, int currentDay) {
		
		if (ci.getInfected() == 0 || ci.getPopulation() == 0) {
			return ci;
		}
		
		int healty = ci.getHealthy();
		int infected = ci.getInfected();

		Random rand = new Random();
		double percDead = deadFactorMin + (deadFactorMax - deadFactorMin) * rand.nextDouble();

		healty -= healty * percDead;
		infected -= infected * percDead;

		ci.setInfected(infected);
		ci.setPopulation(infected + healty);
		
		return ci;
	}

}
