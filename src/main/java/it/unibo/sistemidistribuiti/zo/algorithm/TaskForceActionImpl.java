package it.unibo.sistemidistribuiti.zo.algorithm;

import java.util.Random;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;

/**
 * Implementazione della task force, in grado di ridurre il numero di infetti
 */
public class TaskForceActionImpl implements TaskForceAction {
	
	private static double tfFactorMin = 1D / 100D;
	private static double tfFactorMax = 1D / 5D;

	@Override
	public CityDAO apply(CityDAO ci, int currentDay) {
		
		if (ci.getInfected() == 0) {
			return ci;
		}
		int infected = ci.getInfected();

		Random rand = new Random();
		double percDead = tfFactorMin + (tfFactorMax - tfFactorMin) * rand.nextDouble();

//		infected -= (infected * percDead + rand.nextInt(20));
		infected -= (10000 * percDead);
			if (infected < 0) {
			infected = 0;
		}
		ci.setInfected(infected);
		
		
		
		return ci;
	}

}
