package it.unibo.sistemidistribuiti.zo.algorithm;

import java.util.List;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import it.unibo.sistemidistribuiti.zo.dao.DailyInfoDAO;
import it.unibo.sistemidistribuiti.zo.dao.MigrationDAO;

public interface Migration {

	List<MigrationDAO> apply(CityDAO start, DailyInfoDAO dailyInfo);

}