package it.unibo.sistemidistribuiti.zo.algorithm;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;

public interface Infection {

	CityDAO apply(CityDAO cityInfo, int day);

}