package it.unibo.sistemidistribuiti.zo.algorithm;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;

public class InfectionImpl implements Infection {

	@Override
	public CityDAO apply(CityDAO ci, int day) {
		if(ci.getInfected() == 0) return ci;
		double healty = ci.getPopulation() - ci.getInfected();
		double cont = 0.001;
		healty *= Math.exp(-cont * ci.getPopulation()/1000 * 1/day);
		healty -= 20;

		ci.setInfected((int) (ci.getPopulation() - healty));
		return ci;
	}
}
