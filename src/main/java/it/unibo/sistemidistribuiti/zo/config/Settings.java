package it.unibo.sistemidistribuiti.zo.config;

public class Settings {
	
	//---- TUCSON NODE SERVICE ----
	public static final String TNS_SERVER = "localhost";
	public static final int TNS_PORT = 20504;
	
	public static final String GOVERNMENT_REACTION_FILE = "reactions/government.rsp";
	public static final String TIMER_REACTION_FILE = "reactions/timer.rsp";
	
	//---- JADE ----
	public static final String MC_HOST = "localhost";	//main container server
	public static final int MC_PORT = 2500; // main container port
	
	//---- SIMULATOR ----
	public static final int TIME_SPEED = 50;	//default time speed simulator
	
	public static final int TASK_FORCE_INFECTED_PERC = 30;	// percentuale massima per la task force
	public static final int QUARANTENE_INFECTED_PERC = 70;	// percentuale massima per la quarantena
	public static final int BOMB_INFECTED_PERC = 90;		// percentuale massima per la bomba
	
	//---- MOBILITY
	public static final int CONTAINER_CREATOR_PORT = 9999;
	
}

