package it.unibo.sistemidistribuiti.zo.utils;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import it.unibo.sistemidistribuiti.zo.agent.BaseAgent;
import it.unibo.sistemidistribuiti.zo.dao.Tuplable;
import jade.core.ServiceException;

/**
 * Classe di Util per la gestione del node service
 * @author Ale
 */
public class TupleUtils {

	public static void out(BaseAgent ag, String tString){
		try {
			LogicTuple tuple = LogicTuple.parse(tString);
			final Out out = new Out(ag.getTucsonTupleCentreId(), tuple);
			ag.getBridge().asynchronousInvocation(out);
		} catch (InvalidLogicTupleException | ServiceException e) {
			e.printStackTrace();
		}
	};

	public static void out(BaseAgent ag, Tuplable dao){
		try {
			LogicTuple tuple = dao.toTuple();
			final Out out = new Out(ag.getTucsonTupleCentreId(), tuple);
			ag.getBridge().asynchronousInvocation(out);
		} catch (InvalidLogicTupleException | ServiceException e) {
			e.printStackTrace();
		}
	};
	
}
