package it.unibo.sistemidistribuiti.zo.utils;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class MessageTemplateFactory {

	public static MessageTemplate getMessageTemplate(MSG message) {
		return getMessageTemplate(message, null);
	}

	public static MessageTemplate getMessageTemplate(MSG message, Integer day) {
		
		String suffix = "";
		
		if (day != null) {
			suffix += "day_" + day;
		}
		if (MSG.START_TRANSFERT == message) {
			return MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchConversationId(MessageConstants.START_TRANSFERT + suffix));
		}
		return null;
	}
}
