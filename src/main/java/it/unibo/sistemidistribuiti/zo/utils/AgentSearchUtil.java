package it.unibo.sistemidistribuiti.zo.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.unibo.sistemidistribuiti.zo.dao.CityDAO;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class AgentSearchUtil {
	
	private static Logger log = LogManager.getLogger();
	
	public static DFAgentDescription[] getAllCities(Agent agent) {
		DFAgentDescription[] ret = null;

		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType(AgentConstants.TYPE_CITY);
		template.addServices(sd);
		try {
			ret = DFService.search(agent, template);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		return ret;
	}

	/**
	 * Recupera un agente città a partire dal cityDAO
	 * @param agent
	 * @param ci
	 * @return
	 */
	public static DFAgentDescription getCity(Agent agent, CityDAO ci) {
		
		if(agent == null || ci == null) {
			log.error("Invocato getCity con parametri invalidi: " + agent + ", " + ci);
			return null;
		}
		
		DFAgentDescription[] ret = null;

		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType(AgentConstants.TYPE_CITY);
		sd.setOwnership(AgentConstants.TYPE_CITY + "_" + ci.getId());
		template.addServices(sd);
		try {
			ret = DFService.search(agent, template);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		if (ret.length == 0) {
			log.error("getCity ha prodotto 0 risultati");
			return null;
		}
		return ret[0];
	}

//	public static DFAgentDescription getTimerAgent(Agent agent) {
//		DFAgentDescription[] ret = null;
//
//		DFAgentDescription template = new DFAgentDescription();
//		ServiceDescription sd = new ServiceDescription();
//		sd.setType(AgentConstants.TYPE_TIMER);
//		template.addServices(sd);
//		try {
//			ret = DFService.search(agent, template);
//		} catch (FIPAException e) {
//			e.printStackTrace();
//		}
//		return ret[0];
//	}
	
//	public static DFAgentDescription getRemoteCreatorAgent(Agent agent) {
//		DFAgentDescription[] ret = null;
//
//		DFAgentDescription template = new DFAgentDescription();
//		ServiceDescription sd = new ServiceDescription();
//		sd.setType(AgentConstants.TYPE_REMOTE_CREATOR);
//		template.addServices(sd);
//		try {
//			ret = DFService.search(agent, template);
//		} catch (FIPAException e) {
//			e.printStackTrace();
//		}
//		return ret[0];
//	}
}
