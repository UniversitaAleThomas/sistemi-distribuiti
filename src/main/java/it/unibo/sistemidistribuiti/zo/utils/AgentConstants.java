package it.unibo.sistemidistribuiti.zo.utils;

public class AgentConstants {
	
	public static final String TYPE_CITY = "CITY";
	public static final String TYPE_TIMER = "TIMER";
	public static final String TYPE_CITY_LOADER = "CITY_LOADER";
	public static final String TYPE_REMOTE_CREATOR = "REMOTE_CREATOR";
	
	public static final String SERVICE_ZOMBIE = "ZOMBIE";
	
}
