package it.unibo.sistemidistribuiti.zo.utils;

import jade.lang.acl.ACLMessage;

public class AclMessageFactory {
	
	public static ACLMessage getAclMessage(MSG message) {
		return getAclMessage(message, null);
	}

	public static ACLMessage getAclMessage(MSG message, Integer day) {
		
		String suffix = "";
		
		if (day != null) {
			suffix += "day_" + day;
		}

		if (MSG.START_TRANSFERT == message) {
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.setLanguage(MessageConstants.LANGUAGE);
			msg.setOntology(MessageConstants.ONTOLOGY);
			msg.setConversationId(MessageConstants.START_TRANSFERT + suffix);
			return msg;
		}
		
		return null;
	}
}
