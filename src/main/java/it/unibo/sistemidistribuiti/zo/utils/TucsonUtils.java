package it.unibo.sistemidistribuiti.zo.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.introspection.tools.InspectorGUI;
import alice.tucson.service.TucsonNodeService;
import it.unibo.sistemidistribuiti.zo.config.Settings;

/**
 * Classe di Util per la gestione del node service
 * 
 * @author Ale
 *
 */
public class TucsonUtils {

	private static Logger log = LogManager.getLogger();
	private static Map<Integer, TucsonNodeService> tnsMap;

	/**
	 * Avvia il tucson node service su localhost
	 * 
	 * @param port
	 *            the listening port for the TuCSoN Node service
	 * @return boolean indicante il successo dell'operazione
	 */
	public static String startNS(int port) {

		if (tnsMap == null) {
			tnsMap = new HashMap<Integer, TucsonNodeService>();
		}

		if (tnsMap.containsKey(port)) {
			log.warn("Un altro tns potrebbe essere attivo su questa porta..");
			// ci proviamo lo stesso
		}

		try {
			TucsonNodeService ns = new TucsonNodeService(port);
			ns.install();
			tnsMap.put(port, ns);
		} catch (Exception e) {
			// E.g. another TuCSoN Node is running on same port.
			e.printStackTrace();
			return null;
		}
		
		log.info("TNS avviato correttamente sulla porta " + port);
		
		String currentHostname = "localhost";
		
		try {
			currentHostname = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return "default@"+ currentHostname +":" + port;
	}

	/**
	 * Stoppa il node service sulla porta specificata
	 * 
	 * @param port
	 *            the listening port for the TuCSoN Node service
	 * @return boolean indicante il successo dell'operazione
	 */
	public static void stopNS(int port) {

		if (tnsMap != null) {
			if (tnsMap.containsKey(port)) {
				tnsMap.get(port).shutdown();
				tnsMap.remove(port);
			} else {
				log.warn("Node service non found for port: " + port);
			}
		}
	}

	/**
	 * Stoppa tutti i node service
	 */
	public static void stopAll() {
		if (tnsMap != null) {
			for (int nodePort : tnsMap.keySet()) {
				stopNS(nodePort);
			}
		}
	}
	
	/**
	 * Avvia un tucson inspector sulla porta di default
	 */
	public static void launchInspector() {
		try {
			new InspectorGUI(new TucsonAgentId("myInspector"), new TucsonTupleCentreId("default",Settings.TNS_SERVER, String.valueOf(Settings.TNS_PORT)));
		} catch (TucsonInvalidAgentIdException | TucsonInvalidTupleCentreIdException e) {
			e.printStackTrace();
		}	
	}
}
