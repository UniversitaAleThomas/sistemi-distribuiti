package it.unibo.sistemidistribuiti.zo.utils;

class MessageConstants {
	
	public static final String LANGUAGE = "ZombieOutbreak";
	public static final String ONTOLOGY = "ZoOntology";

	/* Conversation IDs */
	public static final String START_TRANSFERT = "START_TRANSFERT";
}


