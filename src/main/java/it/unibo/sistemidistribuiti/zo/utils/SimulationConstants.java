package it.unibo.sistemidistribuiti.zo.utils;

/**
 * Costanti utilizzate per la gestione degli stati della simulazione
 */
public class SimulationConstants {
	
	public static final int START = 1;
	public static final int PAUSE = 2;
	public static final int STOP = 3;
	public static final int RESUME = 4;
	
}


