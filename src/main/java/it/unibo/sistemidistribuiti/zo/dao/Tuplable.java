package it.unibo.sistemidistribuiti.zo.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;

public interface Tuplable {
	LogicTuple toTuple() throws InvalidLogicTupleException;
//	LogicTuple getTupleTemplate() throws InvalidLogicTupleException;
	void fillFromTuple(LogicTuple tuple);
}
