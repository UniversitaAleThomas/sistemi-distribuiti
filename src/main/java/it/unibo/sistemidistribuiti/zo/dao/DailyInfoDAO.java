package it.unibo.sistemidistribuiti.zo.dao;

import java.io.Serializable;
import java.util.List;

public class DailyInfoDAO implements Serializable {
	private static final long serialVersionUID = -775889312255327118L;

	private int day;
	private List<CityDAO> citiesInfo;

	public DailyInfoDAO(int day, List<CityDAO> citiesInfo) {
		super();
		this.day = day;
		this.citiesInfo = citiesInfo;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public List<CityDAO> getCitiesInfo() {
		return citiesInfo;
	}

	public void setCitiesInfo(List<CityDAO> citiesInfo) {
		this.citiesInfo = citiesInfo;
	}
}
