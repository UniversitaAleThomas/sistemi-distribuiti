package it.unibo.sistemidistribuiti.zo.dao;

import java.io.Serializable;

public class MigrationDAO implements Serializable {
	private static final long serialVersionUID = 3888879498085606413L;

	private int healty;
	private int infected;
	private int tripDuration;
	private CityDAO start;
	private CityDAO destination;

	public MigrationDAO(int healty, int infected, int tripDuration, CityDAO start, CityDAO destination) {
		this.healty = healty;
		this.infected = infected;
		this.tripDuration = tripDuration;
		this.start = start;
		this.destination = destination;
	}

	public int getHealty() {
		return healty;
	}

	public void setHealty(int healty) {
		this.healty = healty;
	}

	public int getInfected() {
		return infected;
	}

	public void setInfected(int infected) {
		this.infected = infected;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public void setTripDuration(int tripDuration) {
		this.tripDuration = tripDuration;
	}

	public CityDAO getStart() {
		return start;
	}

	public void setStart(CityDAO start) {
		this.start = start;
	}

	public CityDAO getDestination() {
		return destination;
	}

	public void setDestination(CityDAO destination) {
		this.destination = destination;
	}

	public int getTotal() {
		return healty + infected;
	}

	@Override
	public String toString() {
		return "Mig: " + getTotal() + "(" + getInfected() + "), Partiti da: " + getStart().getName()+ ", Diretti a: " + getDestination().getName() + ", Durata: " + getTripDuration();
	}
}