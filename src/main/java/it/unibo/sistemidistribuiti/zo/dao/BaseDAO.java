package it.unibo.sistemidistribuiti.zo.dao;

import java.io.Serializable;

import alice.logictuple.LogicTuple;

public abstract class BaseDAO implements Serializable, Tuplable {
	private static final long serialVersionUID = -3131477869418759018L;

	public BaseDAO() {}
	
	public BaseDAO(LogicTuple tuple) {
		fillFromTuple(tuple);
	}
		
}
