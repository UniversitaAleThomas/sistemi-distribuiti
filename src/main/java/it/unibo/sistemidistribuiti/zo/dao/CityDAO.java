package it.unibo.sistemidistribuiti.zo.dao;

import java.awt.Point;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;

public class CityDAO extends BaseDAO {

	private static final long serialVersionUID = 2111249063506767756L;

	public static final int HEALTY = 0;
	public static final int INFECTED = 1;
	public static final int QUARANTINE = 2;
	public static final int NUCLEARIZED = 3;

	public static final String TEMPLATE = "ci(name(N),country(C),population(P),infected(I),state(S),taskForce(TF),coordinate(CX,CY))";

	private String name, country;
	private int population, infected;
	private Point coordinate;
	private int state;

	public boolean taskForce;

	public CityDAO(String name, String country, int population, Point coordinate) {
		super();
		this.name = name;
		this.country = country;
		this.population = population;
		this.coordinate = coordinate;
		this.infected = 0;
		this.state = HEALTY;
	}

	public CityDAO(LogicTuple tuple) {
		super(tuple);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPopulation() {
		if (NUCLEARIZED == state) {
			return 0;
		}
		return population;
	}

	public void setPopulation(int population) {
		if (NUCLEARIZED == state) {
			population = 0;
		}
		this.population = population;
	}

	public int getInfected() {
		if (NUCLEARIZED == state) {
			return 0;
		}
		return infected;
	}

	public void setInfected(int infected) {
		if (infected > 0 && state == HEALTY) {
			state = INFECTED;
		} else if (infected == 0 && state == INFECTED) {
			state = HEALTY;
		} else if (NUCLEARIZED == state) {
			infected = 0;
		}

		this.infected = infected;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "City: " + name + ", population: " + population + ", infected: " + infected;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Point getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Point coordinate) {
		this.coordinate = coordinate;
	}

	public String getCompleteName() {
		return country + "_" + name;
	}

	public int getId() {
		return getCompleteName().hashCode();
	}

	public void nuclearize() {
		infected = 0;
		population = 0;
		state = NUCLEARIZED;
	}

	public double getInfectedPercentage() {
		return (double) infected / (double) population * 100;
	}

	@Override
	public boolean equals(Object obj) {
		return this.getCompleteName().equals(((CityDAO) obj).getCompleteName());
	}

	@Override
	public int hashCode() {
		return this.getCompleteName().hashCode();
	}

	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		int taskForceInt = 0;
		if (this.taskForce)
			taskForceInt = 1;
		return LogicTuple.parse("ci(name(" + getName() + "),country(" + getCountry() + "),population(" + getPopulation()
				+ "),infected(" + getInfected() + "),state(" + getState() + "),taskForce(" + taskForceInt
				+ "),coordinate(" + (int) getCoordinate().getX() + "," + (int) getCoordinate().getY() + "))");
	}

	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.name = tuple.getArg("name").getArg(0).toString();
		this.country = tuple.getArg("country").getArg(0).toString();
		this.population = tuple.getArg("population").getArg(0).intValue();
		this.infected = tuple.getArg("infected").getArg(0).intValue();
		this.state = tuple.getArg("state").getArg(0).intValue();
		int x = tuple.getArg("coordinate").getArg(0).intValue();
		int y = tuple.getArg("coordinate").getArg(1).intValue();
		int taskForce = tuple.getArg("taskForce").getArg(0).intValue();
		this.taskForce = taskForce != 0;
		this.coordinate = new Point(x, y);
	}

	public boolean isNuclearized() {
		return state == NUCLEARIZED;
	}

	public void quarantinize() {
		state = QUARANTINE;
	}

	public void addTaskForce() {
		taskForce = true;
	}

	public boolean containsTaskForce() {
		return taskForce;
	}

	public boolean isQuarantine() {
		return state == QUARANTINE;
	}

	public void removeTaskForce() {
		taskForce = false;
	}

	public void dequarantinize() {
		state = INFECTED;
	}

	public int getHealthy() {
		if (NUCLEARIZED == state) {
			return 0;
		}
		return population - infected;
	}
}
