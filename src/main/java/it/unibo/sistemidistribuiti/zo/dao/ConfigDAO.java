package it.unibo.sistemidistribuiti.zo.dao;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;

public class ConfigDAO extends BaseDAO {

	private static final long serialVersionUID = -6863851704484394539L;

	public static final String TEMPLATE = "config(time_speed(X),quarantinePerc(W),taskForcePerc(Y),bombPerc(Z))";
	private int timeSpeed;
	private int quarantinePercentage;
	private int taskForcePercentage;
	private int atomicPercentage;

	public ConfigDAO(int timeSpeed, int quarantinePercentage, int taskForcePercentage, int atomicPercentage) {
		super();
		this.timeSpeed = timeSpeed;
		this.quarantinePercentage = quarantinePercentage;
		this.taskForcePercentage = taskForcePercentage;
		this.atomicPercentage = atomicPercentage;
	}

	public ConfigDAO(LogicTuple tupla) {
		super(tupla);
	}

	public int getTimeSpeed() {
		return timeSpeed;
	}

	public void setTimeSpeed(int timeSpeed) {
		this.timeSpeed = timeSpeed;
	}

	public int getQuarantinePercentage() {
		return quarantinePercentage;
	}

	public void setQuarantinePercentage(int quarantinePercentage) {
		this.quarantinePercentage = quarantinePercentage;
	}

	public int getTaskForcePercentage() {
		return taskForcePercentage;
	}

	public void setTaskForcePercentage(int taskForcePercentage) {
		this.taskForcePercentage = taskForcePercentage;
	}

	public int getAtomicPercentage() {
		return atomicPercentage;
	}

	public void setAtomicPercentage(int atomicPercentage) {
		this.atomicPercentage = atomicPercentage;
	}

	@Override
	public LogicTuple toTuple() throws InvalidLogicTupleException {
		return LogicTuple.parse("config(time_speed(" + timeSpeed + "),quarantinePerc(" + quarantinePercentage
				+ "),taskForcePerc(" + taskForcePercentage + "),bombPerc(" + atomicPercentage + "))");
	}

	@Override
	public void fillFromTuple(LogicTuple tuple) {
		this.timeSpeed = tuple.getArg("time_speed").getArg(0).intValue();
		this.quarantinePercentage = tuple.getArg("quarantinePerc").getArg(0).intValue();
		this.taskForcePercentage = tuple.getArg("taskForcePerc").getArg(0).intValue();
		this.atomicPercentage = tuple.getArg("bombPerc").getArg(0).intValue();
	}
}
