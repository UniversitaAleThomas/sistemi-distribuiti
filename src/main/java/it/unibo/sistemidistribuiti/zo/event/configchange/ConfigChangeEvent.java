package it.unibo.sistemidistribuiti.zo.event.configchange;

import it.unibo.sistemidistribuiti.zo.dao.ConfigDAO;
import it.unibo.sistemidistribuiti.zo.event.Event;

public class ConfigChangeEvent implements Event{
	
	private ConfigDAO config;
	
	public ConfigChangeEvent(ConfigDAO config) {
		super();
		this.config = config;
	}
	
	@Override
	public String getType() {
		return "ConfigChangeEvent";
	}

	public ConfigDAO getConfig() {
		return config;
	}

	public void setConfig(ConfigDAO config) {
		this.config = config;
	}


}
