package it.unibo.sistemidistribuiti.zo.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

@SuppressWarnings("rawtypes")
public class EventDispatcher {
	private static EventDispatcher singleton;
	private Map<Class<? extends Event>, List<EventHandler>> eventsBus;

	private EventDispatcher() {
		eventsBus = new WeakHashMap<>();
	}

	public static EventDispatcher get() {
		if (singleton == null) {
			singleton = new EventDispatcher();
		}
		return singleton;
	}

	public <T extends Event> void addEventHandler(EventHandler<T> handler) {
		Class<T> eventType = handler.getType();
		if (eventsBus.get(eventType) == null) {
			eventsBus.put(eventType, new ArrayList<>());
		}
		eventsBus.get(eventType).add(handler);
	}

	@SuppressWarnings("unchecked")
	public <T extends Event> void fireEvent(T event) {
		List<EventHandler> handlers = eventsBus.get(event.getClass());
		if (handlers != null) {
			for (EventHandler<T> h : handlers) {
				h.run(event);
			}
		}
	}

}
