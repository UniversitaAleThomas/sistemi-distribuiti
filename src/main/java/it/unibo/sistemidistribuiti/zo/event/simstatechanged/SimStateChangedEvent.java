package it.unibo.sistemidistribuiti.zo.event.simstatechanged;

import it.unibo.sistemidistribuiti.zo.core.ISimulator.SIM_STATE;
import it.unibo.sistemidistribuiti.zo.event.Event;

public class SimStateChangedEvent implements Event{
	
	private SIM_STATE state;
	
	@Override
	public String getType() {
		return "SimStateChangedEvent";
	}
	
	public SimStateChangedEvent(SIM_STATE state) {
		super();
		this.setState(state);
	}

	public SIM_STATE getState() {
		return state;
	}

	public void setState(SIM_STATE state) {
		this.state = state;
	}


}
