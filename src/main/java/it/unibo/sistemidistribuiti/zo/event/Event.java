package it.unibo.sistemidistribuiti.zo.event;

public interface Event {
	public String getType();
}
