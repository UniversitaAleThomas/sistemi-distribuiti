package it.unibo.sistemidistribuiti.zo.event;

public interface EventHandler<T extends Event> {
	public Class<T> getType();
	public void run(T event);
}
