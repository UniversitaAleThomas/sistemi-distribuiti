package it.unibo.sistemidistribuiti.zo.event.simstatechanged;

import it.unibo.sistemidistribuiti.zo.event.BaseEventHandler;

public abstract class SimStateChangedEventHandler extends BaseEventHandler<SimStateChangedEvent> {

	public SimStateChangedEventHandler() {
		super(SimStateChangedEvent.class);
	}

	@Override
	public abstract void run(SimStateChangedEvent event);
	
}
