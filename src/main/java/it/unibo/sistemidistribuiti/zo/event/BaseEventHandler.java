package it.unibo.sistemidistribuiti.zo.event;

public abstract class BaseEventHandler<T extends Event> implements EventHandler<T> {
	private Class<T> type;

	public BaseEventHandler(Class<T> clazz) {
		this.type = clazz;
	}

	@Override
	public Class<T> getType() {
		return this.type;
	}
}
