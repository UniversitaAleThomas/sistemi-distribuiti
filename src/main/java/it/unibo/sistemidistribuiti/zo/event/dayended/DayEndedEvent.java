package it.unibo.sistemidistribuiti.zo.event.dayended;

import it.unibo.sistemidistribuiti.zo.dao.DailyInfoDAO;
import it.unibo.sistemidistribuiti.zo.event.Event;

public class DayEndedEvent implements Event{
	
	private DailyInfoDAO dailyInfo ;
	private int travelers;
	
	@Override
	public String getType() {
		return "DayEndedEvent";
	}
	public DayEndedEvent(DailyInfoDAO dailyInfo, int travelers) {
		super();
		this.dailyInfo = dailyInfo;
		this.setTravelers(travelers);
	}
	public DailyInfoDAO getDailyInfo() {
		return dailyInfo;
	}

	public void setDailyInfo(DailyInfoDAO dailyInfo) {
		this.dailyInfo = dailyInfo;
	}
	public int getTravelers() {
		return travelers;
	}
	public void setTravelers(int travelers) {
		this.travelers = travelers;
	}

}
